defmodule QMWeb do
  @moduledoc """
  The entrypoint for defining your web interface, such
  as controllers, views, channels and so on.

  This can be used in your application as:

      use QMWeb, :controller
      use QMWeb, :view

  The definitions below will be executed for every view,
  controller, etc, so keep them short and clean, focused
  on imports, uses and aliases.

  Do NOT define functions inside the quoted expressions
  below. Instead, define any helper function in modules
  and import those modules here.
  """

  def controller do
    quote do
      use Phoenix.Controller, namespace: QMWeb

      import Phoenix.LiveView.Controller
      import Plug.Conn
      import QMWeb.Gettext
      import QMWeb.ControllerHelpers
      alias QMWeb.Router.Helpers, as: Routes
    end
  end

  def view do
    quote do
      use Phoenix.View,
        root: "lib/quilted_memoir_web/templates",
        namespace: QMWeb

      # Import convenience functions from controllers
      import Phoenix.Controller, only: [get_flash: 1, get_flash: 2, view_module: 1]

      # Use all HTML functionality (forms, tags, etc)
      use Phoenix.HTML

      import Phoenix.LiveView.Helpers

      import QMWeb.ErrorHelpers
      import QMWeb.Gettext
      alias QMWeb.Router.Helpers, as: Routes

      def flash_color_for_style("success"), do: "bg-green-600 text-white"
      def flash_color_for_style("info"), do: "bg-blue-600 text-white"
      def flash_color_for_style("error"), do: "bg-red-600 text-white"
    end
  end

  def router do
    quote do
      use Phoenix.Router
      import Plug.Conn
      import Phoenix.Controller
      import Phoenix.LiveView.Router
    end
  end

  def channel do
    quote do
      use Phoenix.Channel
      import QMWeb.Gettext
    end
  end

  @doc """
  When used, dispatch to the appropriate controller/view/etc.
  """
  defmacro __using__(which) when is_atom(which) do
    apply(__MODULE__, which, [])
  end
end
