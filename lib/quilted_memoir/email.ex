defmodule QM.Email do
  @moduledoc """
  Primary interface for generating emails for Quilted Memoir
  """

  use Bamboo.Phoenix, view: QMWeb.EmailView

  alias QMWeb.Router.Helpers, as: Routes

  @reply_to_email "hello@quiltedmemoir.com"

  def welcome_email(email) do
    base_email()
    |> to(email)
    |> subject("Welcome to Quilted Memoir")
    |> render(:welcome)
  end

  def password_reset_requested(email, token, _expiration) do
    password_reset_url =
      Routes.account_url(QMWeb.Endpoint, :finish_password_reset_get, token: token)

    base_email()
    |> to(email)
    |> subject("Password Reset Requested")
    |> render(:reset_password, password_reset_url: password_reset_url)
  end

  def contributor_invitation(
        quilt_id,
        inviting_user_id,
        invited_user_id,
        quilt_subject,
        invitation_prompt \\ nil
      ) do
    invitation_prompt =
      case is_nil(invitation_prompt) do
        true ->
          """
          I’m collecting stories about #{quilt_subject} using Quilted Memoir.
          Specifically, I’m looking for the stories that only you would be able to tell.
          What do you remember that you think others might want to know about?
          """

        _ ->
          invitation_prompt
      end

    contributor_link =
      Routes.quilt_url(QMWeb.Endpoint, :add_memory_guest_get, quilt_id, invited_user_id)

    quilted_memoir_url = Routes.url(QMWeb.Endpoint)

    with %{email: from_email} <- user_id_to_email(inviting_user_id, :from_email),
         %{email: to_email} <- user_id_to_email(invited_user_id, :to_email) do
      base_email()
      |> to(to_email)
      |> subject("Memories about #{quilt_subject}")
      |> render(:contributor_invitation,
        from_email: from_email,
        quilt_subject: quilt_subject,
        invitation_prompt: invitation_prompt,
        contributor_link: contributor_link,
        quilted_memoir_url: quilted_memoir_url
      )
    else
      {:error, :account_not_found, recipient_type} -> {:error, :account_not_found, recipient_type}
    end
  end

  defp user_id_to_email(user_id, recipient_type) do
    QM.Accounts.get_account!(user_id)
    |> case do
      nil -> {:error, :account_not_found, recipient_type}
      %QM.Schemas.Account{} = account -> account
    end
  end

  defp base_email do
    # Here you can set a default from, default headers, etc.
    new_email()
    |> from("hello@quiltedmemoir.com")
    |> put_header("Reply-To", @reply_to_email)
    |> put_html_layout({QMWeb.LayoutView, "email.html"})
    |> put_text_layout({QMWeb.LayoutView, "email.text"})
  end
end
