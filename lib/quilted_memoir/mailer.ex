defmodule QM.Mailer do
  @moduledoc """
  Mailerinterface for bamboo
  """

  use Bamboo.Mailer, otp_app: :quilted_memoir
end
