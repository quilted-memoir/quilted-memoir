defmodule QM.Release do
  @moduledoc """
  Encapsulate tasks to run when released, like migrations
  """

  @app :quilted_memoir

  alias EventStore.Tasks.Create, as: CreateEventStore
  alias EventStore.Tasks.Init, as: InitEventStore

  def all_tasks do
    :ok = Application.load(@app)

    init_event_store()
    migrate()
  end

  def init_event_store do
    {:ok, _} = Application.ensure_all_started(:postgrex)
    {:ok, _} = Application.ensure_all_started(:ssl)

    :ok = Application.load(:eventstore)

    config = EventStore.Config.parsed(QMES.EventStore, :quilted_memoir)

    :ok = CreateEventStore.exec(config, [])
    :ok = InitEventStore.exec(QMES.EventStore, config, [])
  end

  def migrate do
    for repo <- repos() do
      {:ok, _, _} = Ecto.Migrator.with_repo(repo, &Ecto.Migrator.run(&1, :up, all: true))
    end
  end

  def rollback(repo, version) do
    {:ok, _, _} = Ecto.Migrator.with_repo(repo, &Ecto.Migrator.run(&1, :down, to: version))
  end

  defp repos do
    Application.fetch_env!(@app, :ecto_repos)
  end
end
