defmodule QM.Schemas.Account do
  @moduledoc """
  Read-model schema for a user account
  """

  use QM.Schema
  import Ecto.Changeset

  schema "accounts" do
    field :name, :string
    field :email, :string
    field :password_hashed, :string
    field :password_reset_token, :string
    field :password_reset_token_expiration, :utc_datetime_usec
  end

  def set_password_reset(account, password_reset_token, password_reset_token_expiration)
      when is_bitstring(password_reset_token_expiration) do
    {:ok, iso_date, _} = DateTime.from_iso8601(password_reset_token_expiration)

    Ecto.Changeset.change(account,
      password_reset_token: password_reset_token,
      password_reset_token_expiration: iso_date
    )
  end

  @spec make_changes :: Ecto.Changeset.t()
  def make_changes do
    %__MODULE__{}
    |> cast(%{}, [:name, :email])
    |> validate_required([:name, :email])
  end
end
