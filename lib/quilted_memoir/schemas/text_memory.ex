defmodule QM.Schemas.TextMemory do
  @moduledoc """
  Read-model schema for a text-based memory
  """

  use QM.Schema

  schema "text_memories" do
    field :quilt_id, :string
    field :user_id, :string
    field :memory, :string
  end
end
