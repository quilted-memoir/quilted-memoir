defmodule QM.Schemas.Quilt do
  @moduledoc """
  Read-model schema for a quilt
  """

  use QM.Schema
  import Ecto.Changeset

  schema "quilts" do
    field :user_id, :string
    field :name, :string
    field :photo_url, :string
  end

  def changeset(quilt, attrs) do
    quilt
    |> cast(attrs, [:name, :photo_url])
    |> validate_required([:name])
  end
end
