defmodule QM.Schemas.QuiltContributor do
  @moduledoc """
  Read-model schema for a contributor invited to a quilt
  """

  use QM.Schema

  schema "quilt_contributors" do
    field :quilt_id, :string
    field :inviting_user_id, :string
    field :invited_contributor_email, :string
  end
end
