defmodule QM.Schemas.QuiltWordFrequency do
  @moduledoc """
  Read-model schema for a quilt
  """

  use QM.Schema
  import Ecto.Changeset

  schema "quilt_word_frequency" do
    field :quilt_id, :string
    field :word, :string
    field :count, :integer
  end

  def changeset(freq, params \\ %{}) do
    freq |> cast(params, [:quilt_id, :word, :count])
  end

  def increase_count(
        %Ecto.Changeset{data: %__MODULE__{count: old_count}} = changeset,
        new_count
      ) do
    changeset |> change(count: old_count + new_count)
  end
end
