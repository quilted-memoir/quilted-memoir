defmodule QM.Accounts do
  @moduledoc """
  Interface for creating and managing accounts
  """

  alias QM.Schemas.Account

  alias QMES.Commands.{
    ExpirePasswordResetToken,
    RegisterAccount,
    RegisterGuest,
    RequestPasswordReset,
    ResetPasswordWithToken
  }

  @spec register_account(String.t(), String.t(), String.t()) ::
          {:error, any()} | {:ok, %Account{}}
  def register_account(email, password, password_again) when password == password_again do
    account_id = UUID.uuid4()

    register_account =
      %{email: email, password: password}
      |> RegisterAccount.new()
      |> RegisterAccount.assign_uuid(account_id)
      |> RegisterAccount.downcase_email()
      |> RegisterAccount.hash_password()

    with :ok <- QMES.Application.dispatch(register_account),
         account <- %Account{email: email, id: account_id, name: nil} do
      {:ok, account}
    else
      {:error, msg} -> {:error, msg}
      {:error, :validation_failure, messages} -> {:error, messages}
      _ -> {:error, :error_registering_account}
    end
  end

  def register_account(_, password, password_again) when password != password_again do
    {:error, :passwords_must_match}
  end

  def register_guest(email) do
    account_id = UUID.uuid4()

    register_guest =
      %{email: email}
      |> RegisterGuest.new()
      |> RegisterGuest.assign_uuid(account_id)
      |> RegisterGuest.downcase_email()

    with :ok <- QMES.Application.dispatch(register_guest, consistency: :strong),
         account <- %Account{email: email, id: account_id} do
      {:ok, account}
    else
      {:error, msg} -> {:error, msg}
      {:error, :validation_failure, messages} -> {:error, messages}
      _ -> {:error, :error_registering_account}
    end
  end

  def login(email, password) do
    with {:ok, account} <- get_account_by_email(email),
         :ok <- check_password(password, account.password_hashed) do
      {:ok, account}
    else
      {:error, :account_not_found} -> login_fail_with_delay()
      :invalid_password -> {:error, :invalid_username_or_password}
    end
  end

  def request_password_reset(email) do
    token = random_string(32)

    {:ok, expiration} =
      Timex.now()
      |> Timex.shift(seconds: password_reset_expiration_seconds())
      |> Timex.format("{ISO:Extended:Z}")

    with {:ok, account} <- get_account_by_email(email),
         :ok <-
           QMES.Application.dispatch(%RequestPasswordReset{
             account_id: account.id,
             token: token,
             expiration: expiration
           }) do
      {:ok, token, expiration}
    else
      {:error, :account_not_found} -> {:ok, token, expiration}
    end
  end

  def password_reset_expiration_seconds do
    15 * 60
  end

  def reset_password(reset_token, new_password) do
    reset_password_command =
      %{token: reset_token, password: new_password}
      |> ResetPasswordWithToken.new()
      |> ResetPasswordWithToken.hash_password()

    with {:ok, account} <- get_account_by_reset_token(reset_token),
         reset_password_command <-
           ResetPasswordWithToken.assign_account_id(reset_password_command, account.id),
         :ok <- QMES.Application.dispatch(reset_password_command) do
      :ok
    else
      {:error, :validation_failure, messages} -> {:error, messages}
      {:error, :account_not_found} -> {:error, :account_not_found}
      {:error, _} -> {:error, :error_resetting_password}
    end
  end

  def expire_password_reset_token(reset_token) do
    with {:ok, account} <- get_account_by_reset_token(reset_token),
         expiration <- DateTime.utc_now(),
         :ok <-
           QMES.Application.dispatch(%ExpirePasswordResetToken{
             account_id: account.id,
             token: reset_token,
             expired_at: expiration
           }) do
      :ok
    else
      {:error, :account_not_found} -> {:error, :account_not_found}
    end
  end

  def get_account(user_id) do
    QM.Repo.get(Account, user_id)
  end

  def get_account!(user_id) do
    QM.Repo.get!(Account, user_id)
  end

  def get_account_by_email(email) do
    case QM.Repo.get_by(Account, email: email) do
      nil -> {:error, :account_not_found}
      account -> {:ok, account}
    end
  end

  defp get_account_by_reset_token(token) do
    case QM.Repo.get_by(Account, password_reset_token: token) do
      nil -> {:error, :account_not_found}
      account -> {:ok, account}
    end
  end

  defp check_password(password, password_hashed) do
    case System.get_env("MIX_ENV") do
      "test" ->
        fake_check_password(password, password_hashed)

      _ ->
        case Argon2.verify_pass(password, password_hashed) do
          true -> :ok
          false -> :invalid_password
        end
    end
  end

  defp fake_check_password(pw1, pw2) do
    case pw1 == pw2 do
      true -> :ok
      false -> :invalid_password
    end
  end

  defp login_fail_with_delay do
    case System.get_env("MIX_ENV") do
      "test" -> nil
      _ -> Argon2.no_user_verify()
    end

    {:error, :invalid_username_or_password}
  end

  defp random_string(length) do
    length |> :crypto.strong_rand_bytes() |> Base.url_encode64() |> binary_part(0, length)
  end
end
