defmodule QM.Quilts do
  @moduledoc """
  Domain interface for working with Quilts
  """

  alias QMES.Storage.Quilts, as: QuiltStorage

  @type start_quilt_function ::
          (user_id :: String.t(), name :: String.t() -> {:ok, %QM.Schemas.Quilt{}})

  @spec start_quilt(
          user_id :: String.t(),
          name :: String.t(),
          start_quilt_function :: start_quilt_function
        ) :: {:ok, %QM.Schemas.Quilt{}}
  def start_quilt(user_id, name, start_quilt_function \\ &QuiltStorage.start_quilt/2) do
    start_quilt_function.(user_id, name)
  end

  @type set_quilt_photo_url_function ::
          (quilt_id :: String.t(), photo_url :: String.t() -> {:ok, %QM.Schemas.Quilt{}})

  @spec set_quilt_photo_url(
          quilt_id :: String.t(),
          photo_url :: String.t(),
          set_quilt_photo_url_function :: set_quilt_photo_url_function
        ) :: {:ok, %QM.Schemas.Quilt{}}
  def set_quilt_photo_url(
        quilt_id,
        photo_url,
        set_quilt_photo_url_function \\ &QuiltStorage.set_quilt_photo_url/2
      ) do
    set_quilt_photo_url_function.(quilt_id, photo_url)
  end

  @type get_quilt_function ::
          (quilt_id :: String.t() -> {:ok, %QM.Schemas.Quilt{}})

  @spec get_quilt(quilt_id :: String.t(), get_quilt_function :: get_quilt_function) ::
          {:ok, %QM.Schemas.Quilt{}}
  def get_quilt(quilt_id, get_quilt_function \\ &QuiltStorage.get_quilt/1) do
    get_quilt_function.(quilt_id)
  end

  @type get_quilts_function ::
          (user_id :: String.t() -> {:ok, [%QM.Schemas.Quilt{}]})

  @spec get_quilts(user_id :: String.t(), get_quilts_function :: get_quilts_function) ::
          {:ok, [%QM.Schemas.Quilt{}]}
  def get_quilts(user_id, get_quilts_function \\ &QuiltStorage.get_quilts/1) do
    get_quilts_function.(user_id)
  end

  @type get_text_memory_function ::
          (memory_id :: String.t() -> {:ok, %QM.Schemas.TextMemory{}})

  @spec get_text_memory(
          memory_id :: String.t(),
          get_text_memory_function :: get_text_memory_function
        ) ::
          {:ok, %QM.Schemas.TextMemory{}}
  def get_text_memory(memory_id, get_text_memory_function \\ &QuiltStorage.get_text_memory/1) do
    get_text_memory_function.(memory_id)
  end

  @type get_text_memories_function ::
          (quilt_id :: String.t() -> {:ok, [%QM.Schemas.TextMemory{}]})

  @spec get_text_memories(
          quilt_id :: String.t(),
          get_text_memories_function :: get_text_memories_function
        ) :: {:ok, [%QM.Schemas.TextMemory{}]}
  def get_text_memories(quilt_id, get_text_memories_function \\ &QuiltStorage.get_text_memories/1) do
    get_text_memories_function.(quilt_id)
  end

  @type add_text_memory_function ::
          (quilt_id :: String.t(), user_id :: String.t(), memory :: String.t() ->
             {:ok, %QM.Schemas.TextMemory{}})

  @spec add_text_memory(
          quilt_id :: String.t(),
          user_id :: String.t() | nil,
          memory :: String.t(),
          add_text_memory_function :: add_text_memory_function
        ) ::
          {:ok, %QM.Schemas.Quilt{}}
  def add_text_memory(
        quilt_id,
        user_id,
        memory,
        add_text_memory_function \\ &QuiltStorage.add_text_memory/3
      ) do
    add_text_memory_function.(quilt_id, user_id, memory)
  end

  @type invite_contributor_function ::
          (quilt_id :: String.t(), inviting_user_id :: String.t(), invited_email :: String.t() ->
             :ok | {:error, :email_already_invited} | {:error, :unknown_error_occurred})

  @spec invite_contributor(
          quilt_id :: String.t(),
          inviting_user_id :: String.t() | nil,
          invited_email :: String.t(),
          invite_contributor_function :: invite_contributor_function
        ) :: :ok | {:error, :email_already_invited} | {:error, :unknown_error_occurred}
  def invite_contributor(
        quilt_id,
        inviting_user_id,
        invited_email,
        invite_contributor_function \\ &QuiltStorage.invite_contributor/3
      ) do
    invite_contributor_function.(
      quilt_id,
      inviting_user_id,
      invited_email
    )
  end

  @type get_word_counts_function ::
          (quilt_id :: String.t() -> {:ok, [%QM.Schemas.QuiltWordFrequency{}]})

  @spec get_word_counts(
          quilt_id :: String.t(),
          get_word_counts_function :: get_word_counts_function
        ) :: any
  def get_word_counts(quilt_id, get_word_counts_function \\ &QuiltStorage.get_word_counts/1) do
    get_word_counts_function.(quilt_id)
  end
end
