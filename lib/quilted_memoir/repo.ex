defmodule QM.Repo do
  use Ecto.Repo,
    otp_app: :quilted_memoir,
    adapter: Ecto.Adapters.Postgres
end
