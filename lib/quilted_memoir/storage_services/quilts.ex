defmodule QM.Storage.Quilts do
  @moduledoc """
  Behaviour for handling the storage and retrieval of quilts
  """

  @doc "Start a new quilt"
  @callback start_quilt(user_id :: String.t(), name :: String.t()) :: {:ok, %QM.Schemas.Quilt{}}

  @doc "Retrieve a quilt"
  @callback get_quilt(quilt_id :: String.t()) :: {:ok, %QM.Schemas.Quilt{}}

  @doc "Retrieve all quilts for a user"
  @callback get_quilts(user_id :: String.t()) :: {:ok, [%QM.Schemas.Quilt{}]}

  @doc "Add a text memory to a quilt"
  @callback add_text_memory(quilt_id :: String.t(), user_id :: String.t(), memory :: String.t()) ::
              {:ok, %QM.Schemas.TextMemory{}}

  @doc "Retrieve a text memory"
  @callback get_text_memory(memory_id :: String.t()) :: {:ok, %QM.Schemas.TextMemory{}}

  @callback get_text_memories(quilt_id :: String.t()) :: {:ok, [%QM.Schemas.TextMemory{}]}

  @doc "Invite someone to contribute to a quilt"
  @callback invite_contributor(
              quilt_id :: String.t(),
              inviting_user_id :: String.t(),
              invited_email :: String.t()
            ) :: :ok | {:error, :email_already_invited} | {:error, :unknown_error_occurred}

  @callback get_word_counts(quilt_id :: String.t()) :: {:ok, [%QM.Schemas.QuiltWordFrequency{}]}
end
