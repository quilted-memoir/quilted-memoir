# Holiness to the Lord

defmodule QM.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  alias Application, as: App

  use Application

  def start(_type, _args) do
    # List all child processes to be supervised
    children = [
      # Start the Ecto repository
      QM.Repo,
      # Start the endpoint when the application starts
      QMWeb.Telemetry,
      QMWeb.Endpoint,
      {Phoenix.PubSub, [name: QM.PubSub, adapter: Phoenix.PubSub.PG2]},
      # Starts a worker by calling: QM.Worker.start_link(arg)
      # {QM.Worker, arg},
      # Start Oban job processing
      {Oban, oban_config()},
      # Event sourcing support
      QMES.Application,
      QMES.Projections.Supervisor,
      QMES.ProcessManagers.Supervisor,
      QMES.EventHandlers.Supervisor,
      {Segment, Application.get_env(:segment, :write_key)}
    ]

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: QM.Supervisor]

    if App.get_env(:quilted_memoir, :enable_sentry, false) do
      {:ok, _} = Logger.add_backend(Sentry.LoggerBackend)
    end

    Supervisor.start_link(children, opts)
  end

  # Tell Phoenix to update the endpoint configuration
  # whenever the application is updated.
  def config_change(changed, _new, removed) do
    QMWeb.Endpoint.config_change(changed, removed)
    :ok
  end

  defp oban_config do
    opts = Application.get_env(:quilted_memoir, Oban)

    # Prevent running queues or scheduling jobs from an iex console.
    if Code.ensure_loaded?(IEx) and IEx.started?() do
      opts
      |> Keyword.put(:crontab, false)
      |> Keyword.put(:queues, false)
    else
      opts
    end
  end
end
