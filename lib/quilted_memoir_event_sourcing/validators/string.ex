defmodule QMES.Support.Validators.String do
  @moduledoc """
  Vex validator to confirm a value is a string
  """
  use Vex.Validator
  alias Vex.Validators.By

  def validate(nil, _options), do: :ok
  def validate("", _options), do: :ok

  def validate(value, _options) do
    By.validate(value, function: &String.valid?/1)
  end
end
