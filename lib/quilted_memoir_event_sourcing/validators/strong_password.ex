defmodule QMES.Support.Validators.StrongPassword do
  @moduledoc """
  Vex validator to confirm a password is valid
  """

  use Vex.Validator

  @password_policy Application.get_env(:quilted_memoir, :password_policy,
                     length: [min: 14],
                     zxcvbn: [
                       min_score: 2
                     ]
                   )
  @weak_password_check Application.get_env(
                         :quilted_memoir,
                         :weak_password_check,
                         :weak_password_check_enabled
                       )
  @pwned_password_check Application.get_env(
                          :quilted_memoir,
                          :pwned_password_check,
                          :pwned_password_check_enabled
                        )

  def validate(value, context) do
    email = Map.get(context, :email)

    with false <- weak_password?(value, [email]),
         false <- pwned_password?(value) do
      :ok
    else
      {:error, message} -> {:error, message}
    end
  end

  defp weak_password?(password, user_inputs) do
    with policy <- Kernel.put_in(@password_policy, [:zxcvbn, :user_inputs], user_inputs),
         :weak_password_check_enabled <- @weak_password_check,
         :ok <- PasswordValidator.validate_password(password, policy) do
      false
    else
      :weak_password_check_disabled -> false
      {:error, [message | _tail]} -> {:error, message}
    end
  end

  defp pwned_password?(password) do
    case ExPwned.password_breached?(password) and
           @pwned_password_check === :pwned_password_check_enabled do
      true -> {:error, "Breached Password"}
      false -> false
    end
  end
end
