defmodule QMES.Support.Validators.ExistingUserId do
  @moduledoc """
  Vex validator to confirm a user ID exists in the system
  """

  use Vex.Validator

  alias QM.Accounts

  def validate(user_id, _) do
    case account_exists?(user_id) do
      true -> :ok
      {:error, message} -> {:error, message}
    end
  end

  defp account_exists?(user_id) do
    case Accounts.get_account(user_id) do
      %{id: ^user_id} -> true
      nil -> {:error, "Account does not exist"}
    end
  end
end
