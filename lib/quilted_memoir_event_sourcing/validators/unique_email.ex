defmodule QMES.Support.Validators.UniqueEmail do
  @moduledoc """
  Vex validator to confirm an email is unique and valid
  """

  use Vex.Validator

  alias QM.Accounts

  def validate(value, context) do
    account_id = Map.get(context, :account_id)

    with true <- valid_domain?(value),
         true <- valid_email?(value),
         false <- email_registered?(value, account_id) do
      :ok
    else
      {:error, message} -> {:error, message}
    end
  end

  defp valid_email?(email) do
    case EmailChecker.valid?(email) do
      true -> true
      false -> {:error, "Invalid email address"}
    end
  end

  defp valid_domain?(email) do
    case Burnex.is_burner?(email) do
      false -> true
      true -> {:error, "Invalid email address"}
    end
  end

  defp email_registered?(email, account_id) do
    case Accounts.get_account_by_email(email) do
      {:ok, %{id: ^account_id}} -> false
      {:error, :account_not_found} -> false
      {:ok, _} -> {:error, "Email address already taken"}
    end
  end
end
