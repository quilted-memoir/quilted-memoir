defmodule QMES.Workers.PasswordResets do
  @moduledoc """
  Worker for handling password reset clearing.

  We schedule these jobs in the password_resets event handler when a password
  reset is requested. When the job runs, we expire the password reset token.
  """
  use Oban.Worker,
    queue: :commanded,
    unique: [
      fields: [:args],
      states: [:available, :scheduled, :executing],
      period: QM.Accounts.password_reset_expiration_seconds()
    ]

  @impl Oban.Worker
  def perform(%{"account_id" => _account_id, "token" => token}, _job) do
    QM.Accounts.expire_password_reset_token(token)
  end
end
