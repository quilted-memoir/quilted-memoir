defmodule QMES.Storage.Quilts do
  @moduledoc """
  Storage service for handling quilts via event sourcing
  """

  @behaviour QM.Storage.Quilts

  alias QMES.Commands.{AddTextMemory, InviteContributor, SetQuiltPhotoUrl, StartQuilt}
  alias QM.Schemas.{Quilt, QuiltWordFrequency, TextMemory}

  import Ecto.Query, only: [from: 2]

  def start_quilt(user_id, name) do
    quilt_id = UUID.uuid4()

    :ok = QMES.Application.dispatch(%StartQuilt{quilt_id: quilt_id, name: name, user_id: user_id})

    get_quilt(quilt_id)
  end

  def set_quilt_photo_url(quilt_id, photo_url) do
    :ok = QMES.Application.dispatch(%SetQuiltPhotoUrl{quilt_id: quilt_id, photo_url: photo_url})
    get_quilt(quilt_id)
  end

  @spec get_quilt(String.t()) :: {:ok, %Quilt{}}
  def get_quilt(quilt_id) do
    {:ok, QM.Repo.get_by!(Quilt, id: quilt_id)}
  end

  @spec get_quilts(String.t()) :: {:ok, [%Quilt{}]}
  def get_quilts(user_id) do
    query =
      from q in "quilts",
        select: {q.id, q.name, q.user_id, q.photo_url},
        where: q.user_id == ^user_id

    quilts =
      QM.Repo.all(query)
      |> Enum.map(fn {id, name, user_id, photo_url} ->
        %QM.Schemas.Quilt{id: id, name: name, photo_url: photo_url, user_id: user_id}
      end)

    {:ok, quilts}
  end

  def add_text_memory(quilt_id, user_id, memory) do
    memory_id = UUID.uuid4()

    :ok =
      QMES.Application.dispatch(%AddTextMemory{
        quilt_id: quilt_id,
        memory_id: memory_id,
        user_id: user_id,
        memory: memory
      })

    get_text_memory(memory_id)
  end

  def get_text_memory(memory_id) do
    {:ok, QM.Repo.get_by!(TextMemory, id: memory_id)}
  end

  def get_text_memories(quilt_id) do
    query = from m in TextMemory, select: m, where: m.quilt_id == ^quilt_id
    {:ok, QM.Repo.all(query)}
  end

  def invite_contributor(
        quilt_id,
        inviting_user_id,
        invited_email
      ) do
    {:ok, %QM.Schemas.Account{id: invited_user_id}} =
      case QM.Accounts.get_account_by_email(invited_email) do
        {:error, :account_not_found} -> QM.Accounts.register_guest(invited_email)
        {:ok, account} -> {:ok, account}
      end

    QMES.Application.dispatch(
      %InviteContributor{
        quilt_id: quilt_id,
        inviting_user_id: inviting_user_id,
        invited_user_id: invited_user_id
      },
      consistency: :strong
    )
    |> case do
      :ok -> :ok
      {:error, :email_already_invited} -> {:error, :email_already_invited}
      _ -> {:error, :unknown_error_occurred}
    end
  end

  def get_word_counts(quilt_id) do
    query =
      from c in QuiltWordFrequency,
        where: c.quilt_id == ^quilt_id,
        order_by: [desc: c.count],
        limit: 8

    {:ok, QM.Repo.all(query)}
  end
end
