defmodule QMES.Application do
  @moduledoc """
  Commanded router for Quilted Memoir
  """

  use Commanded.Application,
    otp_app: :quilted_memoir,
    event_store: [
      adapter: Commanded.EventStore.Adapters.EventStore,
      event_store: QMES.EventStore
    ],
    pubsub: :local,
    registry: :local

  router(QMES.Router)
end
