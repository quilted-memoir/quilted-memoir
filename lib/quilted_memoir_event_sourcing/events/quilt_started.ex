defmodule QMES.Events.QuiltStarted do
  @moduledoc """
  Reflects that a quilt has been started by a user
  """
  @derive Jason.Encoder
  defstruct [:quilt_id, :user_id, :name]
end
