defmodule QMES.Events.QuiltPhotoUrlChanged do
  @moduledoc """
  The photo for the quilt has changed
  """
  @derive Jason.Encoder
  defstruct [
    :quilt_id,
    :photo_url
  ]
end
