defmodule QMES.Events.GuestRegistered do
  @moduledoc """
  Records that an account has been registered for a guest
  """
  @derive Jason.Encoder
  defstruct [:account_id, :email]
end
