defmodule QMES.Events.PasswordResetRequested do
  @moduledoc """
  Records that a password reset has been requested for an account
  """
  @derive Jason.Encoder
  defstruct [:account_id, :password_reset_token, :password_reset_token_expiration]
end
