defmodule QMES.Events.PasswordResetTokenExpired do
  @moduledoc """
  Records that an account's password has been reset
  """
  @derive {Jason.Encoder, only: [:account_id]}
  defstruct [
    :account_id,
    :password_reset_token
  ]
end
