defmodule QMES.Events.PasswordResetWithToken do
  @moduledoc """
  Records that an account's password has been reset
  """
  @derive Jason.Encoder
  defstruct [
    :account_id,
    :password_reset_token,
    :new_password_hash
  ]
end
