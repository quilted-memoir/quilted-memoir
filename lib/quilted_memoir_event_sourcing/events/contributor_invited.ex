defmodule QMES.Events.ContributorInvited do
  @moduledoc """
  Records that a contributor has been invited to a given quilt
  """
  @derive {Jason.Encoder, only: [:quilt_id, :inviting_user_id, :invited_user_id]}
  defstruct [:quilt_id, :inviting_user_id, :invited_user_id]
end
