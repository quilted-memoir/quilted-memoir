defmodule QMES.Events.TextMemoryAdded do
  @moduledoc """
  Reflects that a quilt has been started by a user
  """
  @derive Jason.Encoder
  defstruct [:quilt_id, :memory_id, :user_id, :memory]
end
