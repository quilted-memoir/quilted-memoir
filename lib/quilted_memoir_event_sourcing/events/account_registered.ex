defmodule QMES.Events.AccountRegistered do
  @moduledoc """
  Records that an account has been registered
  """
  @derive Jason.Encoder
  defstruct [:account_id, :email, :password_hashed]
end
