defmodule QMES.Router do
  @moduledoc """
  Central command router for Quilted Memoir

  We may split this up as the app gets bigger, but for now we're keeping it in one place.
  """

  use Commanded.Commands.Router

  alias QMES.Aggregates.{Account, Quilt, TextMemory}

  alias QMES.Commands.{
    AddTextMemory,
    ExpirePasswordResetToken,
    InviteContributor,
    RegisterAccount,
    RegisterGuest,
    RequestPasswordReset,
    ResetPasswordWithToken,
    SetQuiltPhotoUrl,
    StartQuilt
  }

  alias QMES.Support.Middleware.Validate

  middleware(Validate)

  # Quilt commands
  dispatch(StartQuilt, to: Quilt, identity: :quilt_id)
  dispatch(InviteContributor, to: Quilt, identity: :quilt_id)
  dispatch(SetQuiltPhotoUrl, to: Quilt, identity: :quilt_id)

  # Memory commands
  dispatch(AddTextMemory, to: TextMemory, identity: :memory_id)

  # Account commands
  dispatch(RegisterAccount, to: Account, identity: :account_id)
  dispatch(RegisterGuest, to: Account, identity: :account_id)
  dispatch(RequestPasswordReset, to: Account, identity: :account_id)
  dispatch(ResetPasswordWithToken, to: Account, identity: :account_id)
  dispatch(ExpirePasswordResetToken, to: Account, identity: :account_id)
end
