defmodule QMES.Commands.ExpirePasswordResetToken do
  @moduledoc """
  Command to expire a password reset token
  """
  @derive {Jason.Encoder, only: [:account_id, :token, :expired_at]}
  defstruct [:account_id, :token, :expired_at]

  use Vex.Struct
  validates(:account_id, presence: true, uuid: true)
  validates(:token, presence: true)
  # validates(:expired_at, presence: true)
end
