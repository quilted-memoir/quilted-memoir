defmodule QMES.Commands.AddTextMemory do
  @moduledoc """
  Command to add a new memory to a quilt
  """
  @derive {Jason.Encoder, only: [:quilt_id, :memory_id, :user_id, :memory]}
  defstruct [:quilt_id, :memory_id, :user_id, :memory]

  use Vex.Struct

  validates(:quilt_id, uuid: true, presence: true)
  validates(:memory_id, uuid: true, presence: true)
  validates(:user_id, uuid: true, presence: true)
  validates(:memory, presence: true, length: [max: 10_000])
end
