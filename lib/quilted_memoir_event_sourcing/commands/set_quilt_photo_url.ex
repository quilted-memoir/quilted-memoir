defmodule QMES.Commands.SetQuiltPhotoUrl do
  @moduledoc """
  Command to set the URL of the quilt's photo
  """
  @derive {Jason.Encoder, only: [:quilt_id, :photo_url]}
  defstruct [:quilt_id, :photo_url]

  use Vex.Struct
end
