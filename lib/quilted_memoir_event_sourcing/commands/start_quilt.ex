defmodule QMES.Commands.StartQuilt do
  @moduledoc """
  Command to indicate that we are starting a new quilt
  """
  @derive {Jason.Encoder, only: [:quilt_id, :user_id, :name]}
  defstruct [:quilt_id, :user_id, :name]

  use Vex.Struct
end
