defmodule QMES.Commands.ResetPasswordWithToken do
  @moduledoc """
  Command to implement a password reset
  """
  @derive {Jason.Encoder, only: [:account_id, :token]}
  defstruct [:account_id, :token, :password, :password_hashed]

  use ExConstructor
  use Vex.Struct

  alias QMES.Support.Validators.StrongPassword

  validates(:account_id, uuid: true)

  validates(:password,
    presence: [message: "can't be empty"],
    string: true,
    by: &StrongPassword.validate/2
  )

  validates(:token, presence: [message: "can't be empty"], string: true)

  validates(:password_hashed, presence: [message: "can't be empty"], string: true)

  @doc """
  Assign a unique identity for the user
  """
  def assign_account_id(%__MODULE__{} = register_account, uuid) do
    %__MODULE__{register_account | account_id: uuid}
  end

  @doc """
  Hash the password, clear the original password
  """
  def hash_password(%__MODULE__{password: password, password_hashed: nil} = register_account)
      when not is_nil(password) do
    %__MODULE__{register_account | password_hashed: hash(password)}
  end

  def hash_password(%__MODULE__{password_hashed: pw} = register_account)
      when not is_nil(pw) do
    register_account
  end

  defp hash(password) do
    case System.get_env("MIX_ENV") do
      "test" -> password
      _ -> Argon2.hash_pwd_salt(password)
    end
  end
end
