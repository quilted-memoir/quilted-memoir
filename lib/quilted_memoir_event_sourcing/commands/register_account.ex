defmodule QMES.Commands.RegisterAccount do
  @moduledoc """
  Command to create a new account
  """
  @derive {Jason.Encoder, only: [:account_id, :email]}
  defstruct [:account_id, :email, :password, :password_hashed]

  use ExConstructor
  use Vex.Struct

  alias QMES.Support.Validators.{StrongPassword, UniqueEmail}

  validates(:account_id, uuid: true)

  validates(:email,
    presence: [message: "can't be empty"],
    format: [with: ~r/\S+@\S+\.\S+/, allow_nil: true, allow_blank: true, message: "is invalid"],
    string: true,
    by: &UniqueEmail.validate/2
  )

  validates(:password,
    presence: [message: "can't be empty"],
    string: true,
    by: &StrongPassword.validate/2
  )

  validates(:password_hashed, presence: [message: "can't be empty"], string: true)

  @doc """
  Assign a unique identity for the user
  """
  def assign_uuid(%__MODULE__{} = register_account, uuid) do
    %__MODULE__{register_account | account_id: uuid}
  end

  @doc """
  Convert email address to lowercase characters
  """
  def downcase_email(%__MODULE__{email: email} = register_account) do
    %__MODULE__{register_account | email: String.downcase(email)}
  end

  @doc """
  Hash the password, clear the original password
  """
  def hash_password(%__MODULE__{password: password, password_hashed: nil} = register_account)
      when not is_nil(password) do
    %__MODULE__{register_account | password_hashed: hash(password)}
  end

  def hash_password(%__MODULE__{password_hashed: pw} = register_account)
      when not is_nil(pw) do
    register_account
  end

  defp hash(password) do
    case System.get_env("MIX_ENV") do
      "test" -> password
      _ -> Argon2.hash_pwd_salt(password)
    end
  end
end
