defmodule QMES.Commands.RegisterGuest do
  @moduledoc """
  Command to register a guest account.
  """
  @derive {Jason.Encoder, only: [:account_id, :email]}
  defstruct [:account_id, :email]

  use ExConstructor
  use Vex.Struct

  alias QMES.Support.Validators.UniqueEmail

  validates(:account_id, uuid: true)

  validates(:email,
    presence: [message: "can't be empty"],
    format: [with: ~r/\S+@\S+\.\S+/, allow_nil: true, allow_blank: true, message: "is invalid"],
    string: true,
    by: &UniqueEmail.validate/2
  )

  @doc """
  Assign a unique identity for the user
  """
  def assign_uuid(%__MODULE__{} = register_account, uuid) do
    %__MODULE__{register_account | account_id: uuid}
  end

  @doc """
  Convert email address to lowercase characters
  """
  def downcase_email(%__MODULE__{email: email} = register_account) do
    %__MODULE__{register_account | email: String.downcase(email)}
  end
end
