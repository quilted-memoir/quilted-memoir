defmodule QMES.Commands.RequestPasswordReset do
  @moduledoc """
  Command to request a password reset
  """
  @derive {Jason.Encoder, only: [:account_id, :token, :expiration]}
  defstruct [:account_id, :token, :expiration]

  use Vex.Struct
end
