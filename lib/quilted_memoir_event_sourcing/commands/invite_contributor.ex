defmodule QMES.Commands.InviteContributor do
  @moduledoc """
  Command to invite a contributor to a quilt
  """
  @derive {Jason.Encoder, only: [:quilt_id, :inviting_user_id, :invited_user_id]}
  defstruct [:quilt_id, :inviting_user_id, :invited_user_id]

  use Vex.Struct

  validates(:quilt_id, presence: true, uuid: true)
  validates(:inviting_user_id, presence: true, uuid: true)
  validates(:invited_user_id, presence: true, uuid: true)
end
