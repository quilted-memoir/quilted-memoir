defmodule QMES.Aggregates.Quilt do
  @moduledoc """
  Aggregate for handling quilt commands
  """
  @enforce_keys [:quilt_id, :user_id]
  defstruct [:quilt_id, :name, :user_id, :photo_url, contributors: []]

  alias __MODULE__
  alias QMES.Commands.{InviteContributor, SetQuiltPhotoUrl, StartQuilt}
  alias QMES.Events.{ContributorInvited, QuiltPhotoUrlChanged, QuiltStarted}

  # Command Handlers

  def execute(%Quilt{quilt_id: nil}, %StartQuilt{
        quilt_id: quilt_id,
        name: name,
        user_id: user_id
      }) do
    %QuiltStarted{quilt_id: quilt_id, name: name, user_id: user_id}
  end

  def execute(%Quilt{quilt_id: id}, %SetQuiltPhotoUrl{
        photo_url: photo_url
      })
      when not is_nil(id) and not is_nil(photo_url) do
    %QuiltPhotoUrlChanged{quilt_id: id, photo_url: photo_url}
  end

  def execute(
        %Quilt{quilt_id: id, contributors: contributors},
        %InviteContributor{invited_user_id: invited_user_id, inviting_user_id: inviting_user_id}
      ) do
    contributors
    |> Enum.member?(%{invited_user_id: invited_user_id, inviting_user_id: inviting_user_id})
    |> case do
      true ->
        {:error, :email_already_invited}

      false ->
        [
          %ContributorInvited{
            quilt_id: id,
            invited_user_id: invited_user_id,
            inviting_user_id: inviting_user_id
          }
        ]
    end
  end

  # State mutators

  def apply(%Quilt{} = quilt, %QuiltStarted{
        quilt_id: quilt_id,
        name: name,
        user_id: user_id
      }) do
    %Quilt{quilt | quilt_id: quilt_id, name: name, user_id: user_id}
  end

  def apply(%Quilt{} = quilt, %QuiltPhotoUrlChanged{
        photo_url: photo_url
      }) do
    %Quilt{quilt | photo_url: photo_url}
  end

  def apply(%Quilt{contributors: contributors} = quilt, %ContributorInvited{
        invited_user_id: invited_user_id,
        inviting_user_id: inviting_user_id
      }) do
    %Quilt{
      quilt
      | contributors: [
          %{invited_user_id: invited_user_id, inviting_user_id: inviting_user_id} | contributors
        ]
    }
  end
end
