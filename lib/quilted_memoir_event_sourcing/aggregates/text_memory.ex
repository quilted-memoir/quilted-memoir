defmodule QMES.Aggregates.TextMemory do
  @moduledoc """
  Text memories capture the memories themselves
  """
  defstruct [
    :memory_id,
    :user_id,
    :quilt_id,
    :memory
  ]

  alias __MODULE__

  alias QMES.Commands.AddTextMemory

  alias QMES.Events.TextMemoryAdded

  # Commands

  def execute(%TextMemory{memory_id: nil}, %AddTextMemory{
        memory_id: memory_id,
        user_id: user_id,
        quilt_id: quilt_id,
        memory: memory
      })
      when not is_nil(memory_id) and not is_nil(user_id) and not is_nil(quilt_id) and
             not is_nil(memory) do
    %TextMemoryAdded{memory_id: memory_id, user_id: user_id, quilt_id: quilt_id, memory: memory}
  end

  # State mutators

  def apply(%TextMemory{} = text_memory, %TextMemoryAdded{
        memory_id: memory_id,
        user_id: user_id,
        quilt_id: quilt_id,
        memory: memory
      }) do
    %TextMemory{
      text_memory
      | memory_id: memory_id,
        user_id: user_id,
        quilt_id: quilt_id,
        memory: memory
    }
  end
end
