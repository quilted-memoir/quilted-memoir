defmodule QMES.Aggregates.Account do
  @moduledoc """
  Aggregate for handling account commands
  """
  defstruct [
    :account_id,
    :email,
    :password_hashed,
    :password_reset_token,
    :password_reset_token_expiration
  ]

  alias __MODULE__

  alias QMES.Commands.{
    ExpirePasswordResetToken,
    RegisterAccount,
    RegisterGuest,
    RequestPasswordReset,
    ResetPasswordWithToken
  }

  alias QMES.Events.{
    AccountRegistered,
    GuestRegistered,
    PasswordResetRequested,
    PasswordResetTokenExpired,
    PasswordResetWithToken
  }

  def execute(%Account{account_id: nil}, %RegisterAccount{
        account_id: account_id,
        email: email,
        password_hashed: password_hashed
      }) do
    %AccountRegistered{account_id: account_id, email: email, password_hashed: password_hashed}
  end

  def execute(%Account{account_id: nil}, %RegisterGuest{
        account_id: account_id,
        email: email
      }) do
    %GuestRegistered{account_id: account_id, email: email}
  end

  def execute(%Account{account_id: nil}, %RequestPasswordReset{}) do
    {:error, :no_such_account}
  end

  def execute(%Account{account_id: id}, %RequestPasswordReset{
        account_id: account_id,
        token: token,
        expiration: expiration
      })
      when not is_nil(id) do
    %PasswordResetRequested{
      account_id: account_id,
      password_reset_token: token,
      password_reset_token_expiration: expiration
    }
  end

  def execute(
        %Account{account_id: id, password_reset_token: reset_token},
        %ResetPasswordWithToken{token: token, password_hashed: new_password_hash}
      )
      when not is_nil(id) and token == reset_token do
    %PasswordResetWithToken{
      account_id: id,
      password_reset_token: token,
      new_password_hash: new_password_hash
    }
  end

  def execute(
        %Account{account_id: id, password_reset_token: reset_token},
        %ExpirePasswordResetToken{token: token}
      )
      when not is_nil(id) and token == reset_token do
    %PasswordResetTokenExpired{
      account_id: id,
      password_reset_token: token
    }
  end

  # State mutators

  def apply(%Account{} = account, %AccountRegistered{
        account_id: account_id,
        email: email,
        password_hashed: password_hashed
      }) do
    %Account{account | account_id: account_id, email: email, password_hashed: password_hashed}
  end

  def apply(%Account{} = account, %GuestRegistered{
        account_id: account_id,
        email: email
      }) do
    %Account{account | account_id: account_id, email: email}
  end

  def apply(%Account{} = account, %PasswordResetRequested{
        password_reset_token: password_reset_token,
        password_reset_token_expiration: password_reset_token_expiration
      }) do
    %Account{
      account
      | password_reset_token: password_reset_token,
        password_reset_token_expiration: password_reset_token_expiration
    }
  end

  def apply(%Account{} = account, %PasswordResetWithToken{
        new_password_hash: new_password_hash
      }) do
    %Account{
      account
      | password_hashed: new_password_hash,
        password_reset_token: nil,
        password_reset_token_expiration: nil
    }
  end

  def apply(%Account{} = account, %PasswordResetTokenExpired{}) do
    %Account{account | password_reset_token: nil, password_reset_token_expiration: nil}
  end
end
