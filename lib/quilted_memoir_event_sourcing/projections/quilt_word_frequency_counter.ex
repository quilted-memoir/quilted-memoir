defmodule QMES.Projections.QuiltWordFrequencyCounter do
  @moduledoc """
  Projection to count word frequency and filter common words
  """

  alias QM.Schemas.QuiltWordFrequency
  alias QMES.Events.TextMemoryAdded
  alias QMES.Projections.QuiltWordFrequencyCounter.Wordlist

  use Commanded.Projections.Ecto,
    application: QMES.Application,
    name: "QMES.Projections.QuiltWordFrequencyCounter"

  project(%TextMemoryAdded{quilt_id: quilt_id, memory: memory}, _metadata, fn multi ->
    query = from c in QuiltWordFrequency, where: c.quilt_id == ^quilt_id

    changesets = QM.Repo.all(query) |> Enum.map(&QuiltWordFrequency.changeset/1)

    [_, multi] =
      (changesets ++ create_entries(memory))
      |> Enum.reduce(%{}, fn entry, acc -> process_entry(acc, entry, quilt_id) end)
      |> Map.values()
      |> Enum.reduce([0, multi], fn changeset, [idx, m] ->
        [
          idx + 1,
          # credo:disable-for-next-line
          Ecto.Multi.insert_or_update(m, String.to_atom("insert_or_update_#{idx}"), changeset)
        ]
      end)

    multi
  end)

  @spec process_entry(
          %{},
          {String.t(), integer} | %QuiltWordFrequency{},
          String.t()
        ) :: %{}
  def process_entry(acc, {word, new_count}, quilt_id) do
    Map.update(
      acc,
      word,
      QuiltWordFrequency.changeset(%QuiltWordFrequency{}, %{
        quilt_id: quilt_id,
        word: word,
        count: new_count
      }),
      fn changeset ->
        QuiltWordFrequency.increase_count(changeset, new_count)
      end
    )
  end

  def process_entry(
        acc,
        %Ecto.Changeset{data: %QuiltWordFrequency{word: word}} = changeset,
        _quilt_id
      ) do
    Map.put(acc, word, changeset)
  end

  @spec create_entries(binary) :: [{String.t(), integer}]
  def create_entries(memory) do
    memory
    |> String.split()
    |> Enum.map(fn word -> trim_word(word) end)
    |> Enum.map(&String.downcase/1)
    |> Enum.reject(&word_is_fluff?/1)
    |> Enum.reduce(%{}, fn word, counters -> Map.update(counters, word, 1, &(&1 + 1)) end)
    |> Map.to_list()
  end

  def trim_word(word) do
    word
    |> String.codepoints()
    |> Enum.reject(&letter_is_punctuation?/1)
    |> Enum.join()
  end

  ".!@#$%^&*()-=+_0987654321;':\"/,?><{}|[]\\"
  |> String.codepoints()
  |> Stream.each(fn letter ->
    def letter_is_punctuation?(unquote(letter)), do: true
  end)
  |> Stream.run()

  def letter_is_punctuation?(_letter), do: false

  Wordlist.words()
  |> Stream.each(fn word -> def word_is_fluff?(unquote(word |> String.downcase())), do: true end)
  |> Stream.run()

  def word_is_fluff?(_word), do: false
end
