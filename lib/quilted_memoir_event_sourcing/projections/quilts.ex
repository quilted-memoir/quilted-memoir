defmodule QMES.Projections.Quilts do
  @moduledoc """
  Projection to generate the list of quilts
  """
  use Commanded.Projections.Ecto,
    application: QMES.Application,
    name: "QMES.Projections.Quilts"

  alias QM.Quilts
  alias QM.Schemas.Quilt
  alias QMES.Events.{QuiltPhotoUrlChanged, QuiltStarted}

  project(%QuiltStarted{quilt_id: quilt_id, user_id: user_id, name: name}, _metadata, fn multi ->
    Ecto.Multi.insert(multi, :quilts, %Quilt{
      id: quilt_id,
      user_id: user_id,
      name: name
    })
  end)

  project(%QuiltPhotoUrlChanged{quilt_id: quilt_id, photo_url: photo_url}, _meta, fn multi ->
    with {:ok, quilt} <- Quilts.get_quilt(quilt_id),
         changeset <- Quilt.changeset(quilt, %{photo_url: photo_url}) do
      Ecto.Multi.update(multi, :quilts, changeset)
    else
      e -> {:error, e}
    end
  end)
end
