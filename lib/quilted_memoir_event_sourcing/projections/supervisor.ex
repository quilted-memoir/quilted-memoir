defmodule QMES.Projections.Supervisor do
  @moduledoc """
  Supervisor for projections
  """
  use Supervisor

  def start_link(_opts) do
    Supervisor.start_link(__MODULE__, nil)
  end

  def init(_) do
    children = [
      # projections
      worker(QMES.Projections.Quilts, [], id: QMES.Projections.Quilts),
      worker(QMES.Projections.Accounts, [], id: QMES.Projections.Accounts),
      worker(QMES.Projections.TextMemories, [], id: QMES.Projections.TextMemories),
      worker(QMES.Projections.QuiltWordFrequencyCounter, [],
        id: QMES.Projections.QuiltWordFrequencyCounter
      )
    ]

    Supervisor.init(children, strategy: :one_for_one)
  end
end
