defmodule QMES.Projections.Accounts do
  @moduledoc """
  Projection to generate the list of quilts
  """
  use Commanded.Projections.Ecto,
    application: QMES.Application,
    name: "QMES.Projections.Accounts"

  alias QMES.Events.{
    AccountRegistered,
    GuestRegistered,
    PasswordResetRequested,
    PasswordResetTokenExpired,
    PasswordResetWithToken
  }

  alias QM.Schemas.Account

  project(
    %AccountRegistered{account_id: account_id, email: email, password_hashed: password_hashed},
    fn multi ->
      Ecto.Multi.insert(multi, :accounts, %Account{
        id: account_id,
        email: email,
        password_hashed: password_hashed
      })
    end
  )

  project(
    %GuestRegistered{account_id: account_id, email: email},
    fn multi ->
      Ecto.Multi.insert(multi, :accounts, %Account{
        id: account_id,
        email: email
      })
    end
  )

  project(
    %PasswordResetRequested{
      account_id: account_id,
      password_reset_token: password_reset_token,
      password_reset_token_expiration: password_reset_token_expiration
    },
    fn multi ->
      changeset =
        Account
        |> QM.Repo.get!(account_id)
        |> Account.set_password_reset(password_reset_token, password_reset_token_expiration)

      Ecto.Multi.update(multi, :accounts_password_reset_requested, changeset)
    end
  )

  project(
    %PasswordResetWithToken{
      account_id: account_id,
      new_password_hash: new_password_hash
    },
    fn multi ->
      account = QM.Repo.get!(Account, account_id)

      changeset =
        Ecto.Changeset.change(account,
          password_reset_token: nil,
          password_reset_token_expiration: nil,
          password_hashed: new_password_hash
        )

      Ecto.Multi.update(multi, :accounts_password_reset, changeset)
    end
  )

  project(%PasswordResetTokenExpired{account_id: account_id}, fn multi ->
    account = QM.Repo.get!(Account, account_id)

    changeset =
      Ecto.Changeset.change(account,
        password_reset_token: nil,
        password_reset_token_expiration: nil
      )

    Ecto.Multi.update(multi, :accounts_password_reset_expired, changeset)
  end)
end
