defmodule QMES.Projections.TextMemories do
  @moduledoc """
  Projection to add text memories for specific quilts
  """
  use Commanded.Projections.Ecto,
    application: QMES.Application,
    name: "QMES.Projections.TextMemories"

  alias QM.Schemas.TextMemory
  alias QMES.Events.TextMemoryAdded

  defstruct [:quilt_id, :memory_id, :user_id, :memory]

  project(
    %TextMemoryAdded{quilt_id: quilt_id, memory_id: memory_id, user_id: user_id, memory: memory},
    _metadata,
    fn multi ->
      Ecto.Multi.insert(multi, :text_memories, %TextMemory{
        id: memory_id,
        quilt_id: quilt_id,
        user_id: user_id,
        memory: memory
      })
    end
  )
end
