defmodule QMES.EventHandlers.ContributorInvitations do
  @moduledoc """
  Event handling for sending emails when contributors are invited to a quilt
  """

  use Commanded.Event.Handler,
    application: QMES.Application,
    name: "ContributorInvitations",
    start_from: :current

  def handle(
        %QMES.Events.ContributorInvited{
          quilt_id: quilt_id,
          inviting_user_id: inviting_user_id,
          invited_user_id: invited_user_id
        },
        _metadata
      )
      when not is_nil(quilt_id) and not is_nil(inviting_user_id) and not is_nil(invited_user_id) do
    with {:ok, %QM.Schemas.Quilt{name: quilt_subject}} <- QM.Quilts.get_quilt(quilt_id) do
      QM.Email.contributor_invitation(quilt_id, inviting_user_id, invited_user_id, quilt_subject)
      |> QM.Mailer.deliver_later()

      :ok
    end
  end
end
