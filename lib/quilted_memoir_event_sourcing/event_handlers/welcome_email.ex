defmodule QMES.EventHandlers.WelcomeEmails do
  @moduledoc """
  Event handling for sending emails when contributors are invited to a quilt
  """

  use Commanded.Event.Handler,
    application: QMES.Application,
    name: "WelcomeEmail",
    start_from: :current

  def handle(
        %QMES.Events.AccountRegistered{
          email: email
        },
        _metadata
      ) do
    QM.Email.welcome_email(email)
    |> QM.Mailer.deliver_later()

    :ok
  end
end
