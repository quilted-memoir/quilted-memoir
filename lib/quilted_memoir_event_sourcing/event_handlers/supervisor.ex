defmodule QMES.EventHandlers.Supervisor do
  @moduledoc """
  Supervisor for event handlers
  """
  use Supervisor

  def start_link(_opts) do
    Supervisor.start_link(__MODULE__, nil)
  end

  def init(_) do
    children = [
      worker(QMES.EventHandlers.WelcomeEmails, [], id: QMES.EventHandlers.WelcomeEmails),
      worker(QMES.EventHandlers.PasswordResets, [], id: QMES.EventHandlers.PasswordResets),
      worker(QMES.EventHandlers.ContributorInvitations, [],
        id: QMES.EventHandlers.ContributorInvitations
      )
    ]

    Supervisor.init(children, strategy: :one_for_one)
  end
end
