defmodule QMES.EventHandlers.PasswordResets do
  @moduledoc """
  Event handling for password reset logic
  """

  use Commanded.Event.Handler,
    application: QMES.Application,
    name: "PasswordResetEmail",
    start_from: :current

  alias QM.Schemas.Account

  import Ecto.Query, only: [from: 2]

  def handle(
        %QMES.Events.PasswordResetRequested{
          account_id: account_id,
          password_reset_token: password_reset_token,
          password_reset_token_expiration: password_reset_token_expiration
        },
        _metadata
      ) do
    send_email(account_id, password_reset_token, password_reset_token_expiration)
    schedule_token_expiration(account_id, password_reset_token, password_reset_token_expiration)

    :ok
  end

  # Remove job if we reset the password in time
  def handle(
        %QMES.Events.PasswordResetWithToken{account_id: account_id, password_reset_token: token},
        _metadata
      ) do
    # Remove the job from the oban queue
    {1, nil} =
      from(j in "oban_jobs",
        where:
          j.args["account_id"] == ^account_id and
            j.args["token"] == ^token
      )
      |> QM.Repo.delete_all()

    :ok
  end

  defp send_email(account_id, token, expiration) do
    case QM.Repo.get!(Account, account_id) do
      %QM.Schemas.Account{email: email} ->
        QM.Email.password_reset_requested(
          email,
          token,
          expiration
        )
        |> QM.Mailer.deliver_later()
    end
  end

  defp schedule_token_expiration(account_id, token, expiration) do
    with {:ok, expiration_time, _utc_offset} <-
           DateTime.from_iso8601(expiration),
         sec_diff <- DateTime.diff(expiration_time, DateTime.utc_now()),
         time_offset <- max(sec_diff, 0),
         expires_at <- DateTime.add(DateTime.utc_now(), time_offset, :second) do
      {:ok, _job} =
        %{account_id: account_id, token: token}
        |> Oban.Job.new(
          scheduled_at: expires_at,
          queue: :commanded,
          worker: QMES.Workers.PasswordResets
        )
        |> Oban.insert()
    end
  end
end
