defmodule QMES.EventStore do
  @moduledoc """
  Event store implementation for Quilted Memoir
  """
  use EventStore, otp_app: :quilted_memoir
end
