defmodule QMES.ProcessManagers.Supervisor do
  @moduledoc """
  Supervisor for process managers
  """
  use Supervisor

  def start_link(_arg) do
    Supervisor.start_link(__MODULE__, [], name: __MODULE__)
  end

  def init(_) do
    Supervisor.init([],
      strategy: :one_for_one
    )
  end
end
