defmodule QMWeb.QuiltController do
  defmodule NewQuiltForm do
    import Ecto.Changeset

    defstruct [:name]

    @types %{name: :string}

    def changeset(name \\ "") do
      {%__MODULE__{}, @types}
      |> cast(%{name: name}, Map.keys(@types))
    end

    def add_name_error(changeset, %{name: [message | _tail]}) do
      changeset
      |> add_error(:name, message)
    end

    def add_name_error(changeset, _) do
      changeset
    end

    def apply_action_insert(changeset) do
      {:error, new_changeset} = apply_action(changeset, :insert)
      new_changeset
    end
  end

  defmodule NewMemoryForm do
    import Ecto.Changeset

    defstruct [:memory]

    @types %{memory: :string}

    def changeset(memory \\ "") do
      {%__MODULE__{}, @types}
      |> cast(%{memory: memory}, Map.keys(@types))
    end

    def add_memory_error(changeset, %{memory: [message | _tail]}) do
      changeset
      |> add_error(:memory, message)
    end

    def add_memory_error(changeset, _) do
      changeset
    end

    def apply_action_insert(changeset) do
      {:error, new_changeset} = apply_action(changeset, :insert)
      new_changeset
    end
  end

  defmodule NewInvitationForm do
    import Ecto.Changeset

    defstruct [:contributor_email]

    @types %{contributor_email: :string}

    def changeset(contributor_email \\ "") do
      {%__MODULE__{}, @types}
      |> cast(%{contributor_email: contributor_email}, Map.keys(@types))
    end

    def add_contributor_email_error(changeset, %{contributor_email: [message | _tail]}) do
      changeset
      |> add_error(:contributor_email, message)
    end

    def add_contributor_email_error(changeset, _) do
      changeset
    end

    def apply_action_insert(changeset) do
      {:error, new_changeset} = apply_action(changeset, :insert)
      new_changeset
    end
  end

  use QMWeb, :controller

  def index(conn, _params) do
    {:ok, quilts} =
      conn
      |> current_user_id()
      |> QM.Quilts.get_quilts()

    quilts =
      quilts
      |> Enum.map(fn %QM.Schemas.Quilt{} = q ->
        {:ok, signed_url} = sign_quilt_photo(q)
        Map.merge(q, %{signed_url: signed_url})
      end)

    render(conn, "index.html", quilts: quilts)
  end

  def new(conn, _params) do
    changeset = NewQuiltForm.changeset()
    callback_url = Routes.quilt_path(conn, :new)

    render(conn, "new.html", changeset: changeset, callback_url: callback_url)
  end

  def new_post(conn, %{"new_quilt_form" => %{"name" => name}})
      when name != "" and
             is_binary(name) do
    # Create the quilt
    {:ok, %QM.Schemas.Quilt{id: id}} = current_user_id(conn) |> QM.Quilts.start_quilt(name)

    # Redirect to the right place
    conn
    |> put_flash(:success, "Quilt Opened")
    |> put_status(:see_other)
    |> redirect(to: Routes.quilt_path(conn, :add_memory_wizard_get, id))
  end

  def new_post(conn, params) do
    conn
    |> put_flash(:error, "You must set a name for this quilt")
    |> new(params)
  end

  ### Wizard methods

  #### Add Memory Wizard

  def add_memory_wizard_get(%{method: "GET"} = conn, %{"id" => _quilt_id} = params) do
    memory_wizard_page(conn, params)
  end

  def add_memory_wizard_post(
        %{method: "POST"} = conn,
        %{
          "id" => quilt_id,
          "new_memory_form" => %{"memory" => text_memory}
        }
      )
      when text_memory != "" and is_binary(text_memory) do
    # Insert the memory
    user_id = conn |> current_user_id()

    {:ok, _quilt} = QM.Quilts.add_text_memory(quilt_id, user_id, text_memory)

    # Direct to the next wizard step
    next_step_url = Routes.quilt_path(conn, :add_invitation_wizard_get, quilt_id)

    conn
    |> put_flash(:success, "Your memory has been added to this quilt!")
    |> redirect(to: next_step_url)
  end

  def add_memory_wizard_post(%{method: "POST"} = conn, params) do
    conn
    |> put_flash(:error, "The memory can't be blank")
    |> memory_wizard_page(params)
  end

  defp memory_wizard_page(conn, %{"id" => quilt_id}) do
    with changeset <- NewMemoryForm.changeset(),
         callback_url <- Routes.quilt_path(conn, :add_memory_wizard_get, quilt_id),
         skip_to_next_step_url <- Routes.quilt_path(conn, :add_invitation_wizard_get, quilt_id),
         {:ok, %{name: name_for_quilt}} <- QM.Quilts.get_quilt(quilt_id) do
      render(conn, "add_memory_wizard.html",
        changeset: changeset,
        callback_url: callback_url,
        skip_to_next_step_url: skip_to_next_step_url,
        name_for_quilt: name_for_quilt
      )
    else
      _ -> conn |> send_resp(404, "Not Found")
    end
  end

  #### Add Memory Guest

  def add_memory_guest_get(
        %{method: "GET"} = conn,
        %{"id" => quilt_id, "guest_id" => guest_id} = params
      ) do
    case guest_not_user(guest_id) do
      :ok ->
        memory_guest_page(conn, params)

      {:error, :unknown_guest} ->
        redirect(conn,
          to: Routes.account_path(conn, :register_guest_get, %{"quilt_id" => quilt_id})
        )

      {:error, :registered_user} ->
        deny_guest(conn)
    end
  end

  def add_memory_guest_get(%{method: "GET"} = conn, %{"id" => quilt_id}) do
    redirect(conn, to: Routes.account_path(conn, :register_guest_get, %{"quilt_id" => quilt_id}))
  end

  def add_memory_guest_post(
        %{method: "POST"} = conn,
        %{
          "id" => quilt_id,
          "guest_id" => guest_id,
          "new_memory_form" => %{"memory" => text_memory}
        }
      )
      when text_memory != "" and is_binary(text_memory) do
    with :ok <- guest_not_user(guest_id),
         {:ok, _quilt} <- QM.Quilts.add_text_memory(quilt_id, guest_id, text_memory),
         next_step_url <- Routes.quilt_path(conn, :add_invitation_guest_get, quilt_id, guest_id) do
      conn
      |> put_flash(:success, "Your memory has been added to this quilt!")
      |> redirect(to: next_step_url)
    else
      {:error, _} -> deny_guest(conn)
    end
  end

  def add_memory_guest_post(%{method: "POST"} = conn, %{"guest_id" => guest_id} = params) do
    case guest_not_user(guest_id) do
      :ok ->
        conn
        |> put_flash(:error, "The memory can't be blank")
        |> memory_guest_page(params)

      {:error, _} ->
        deny_guest(conn)
    end
  end

  defp memory_guest_page(conn, %{"id" => quilt_id, "guest_id" => guest_id}) do
    with changeset <- NewMemoryForm.changeset(),
         callback_url <- Routes.quilt_path(conn, :add_memory_guest_get, quilt_id, guest_id),
         skip_to_next_step_url <-
           Routes.quilt_path(conn, :add_invitation_guest_get, quilt_id, guest_id),
         {:ok, %{name: name_for_quilt}} <- QM.Quilts.get_quilt(quilt_id) do
      render(conn, "add_memory_guest.html",
        changeset: changeset,
        callback_url: callback_url,
        skip_to_next_step_url: skip_to_next_step_url,
        name_for_quilt: name_for_quilt
      )
    else
      _ -> conn |> send_resp(404, "Not Found")
    end
  end

  defp guest_not_user(guest_id) do
    case QM.Accounts.get_account(guest_id) do
      nil -> {:error, :unknown_guest}
      %{:password_hashed => nil} -> :ok
      _ -> {:error, :registered_user}
    end
  end

  defp deny_guest(conn) do
    conn |> put_flash(:error, "Please login to view that page") |> redirect(to: "/")
  end

  #### Add Invitation Wizard

  def add_invitation_wizard(%{method: "GET"} = conn, %{"id" => _quilt_id} = params) do
    invitation_wizard_page(conn, params)
  end

  def add_invitation_wizard(
        %{method: "POST"} = conn,
        %{
          "id" => quilt_id,
          "new_invitation_form" => %{"contributor_email" => contributor_email}
        }
      )
      when contributor_email != "" and is_binary(contributor_email) do
    # Invite the contributor
    user_id = conn |> current_user_id()

    result = QM.Quilts.invite_contributor(quilt_id, user_id, contributor_email)

    # Direct to the next wizard step
    next_step_url = Routes.quilt_path(conn, :add_invitation_wizard_get, quilt_id)

    {flash_style, flash_message} =
      case result do
        :ok ->
          {:success,
           "Thank you for inviting someone to contribute! Invite as many others as you like!"}

        {:error, :email_already_invited} ->
          {:success,
           "Thank you for inviting someone to contribute! Invite as many others as you like!"}

        _ ->
          {:error, "An error occurred adding your invitation. Please try again."}
      end

    conn
    |> put_flash(flash_style, flash_message)
    |> redirect(to: next_step_url)
  end

  def add_invitation_wizard(%{method: "POST"} = conn, params) do
    conn
    |> put_flash(:error, "The invitation email can't be blank")
    |> invitation_wizard_page(params)
  end

  defp invitation_wizard_page(conn, %{"id" => quilt_id}) do
    with changeset <- NewInvitationForm.changeset(),
         callback_url <- Routes.quilt_path(conn, :add_invitation_wizard_get, quilt_id),
         skip_to_next_step_url <- Routes.quilt_path(conn, :view_quilt, quilt_id),
         social_share_url <- Routes.quilt_path(conn, :view_quilt, quilt_id),
         {:ok, %{name: name_for_quilt}} <- QM.Quilts.get_quilt(quilt_id) do
      render(conn, "invitation_wizard.html",
        changeset: changeset,
        callback_url: callback_url,
        quilt_id: quilt_id,
        skip_to_next_step_url: skip_to_next_step_url,
        social_share_url: social_share_url,
        name_for_quilt: name_for_quilt
      )
    else
      _ -> conn |> send_resp(404, "Not Found")
    end
  end

  ### Add Invitation Guest

  def add_invitation_guest(%{method: "GET"} = conn, %{"id" => _quilt_id} = params) do
    invitation_guest_page(conn, params)
  end

  def add_invitation_guest(
        %{method: "POST"} = conn,
        %{
          "id" => quilt_id,
          "guest_id" => guest_id,
          "new_invitation_form" => %{"contributor_email" => contributor_email}
        }
      )
      when contributor_email != "" and is_binary(contributor_email) do
    # Invite the contributor

    result = QM.Quilts.invite_contributor(quilt_id, guest_id, contributor_email)

    # Direct to the next guest step
    next_step_url = Routes.quilt_path(conn, :add_invitation_guest_get, quilt_id, guest_id)

    {flash_style, flash_message} =
      case result do
        :ok ->
          {:success,
           "Thank you for inviting someone to contribute! Invite as many others as you like!"}

        {:error, :email_already_invited} ->
          {:success,
           "Thank you for inviting someone to contribute! Invite as many others as you like!"}

        _ ->
          {:error, "An error occurred adding your invitation. Please try again."}
      end

    conn
    |> put_flash(flash_style, flash_message)
    |> redirect(to: next_step_url)
  end

  def add_invitation_guest(%{method: "POST"} = conn, params) do
    conn
    |> put_flash(:error, "The invitation email can't be blank")
    |> invitation_guest_page(params)
  end

  defp invitation_guest_page(conn, %{"id" => quilt_id, "guest_id" => guest_id}) do
    with changeset <- NewInvitationForm.changeset(),
         callback_url <- Routes.quilt_path(conn, :add_invitation_guest_get, quilt_id, guest_id),
         skip_to_next_step_url <- Routes.quilt_path(conn, :view_quilt, quilt_id),
         social_share_url <- Routes.quilt_path(conn, :view_quilt, quilt_id),
         {:ok, %{name: name_for_quilt}} <- QM.Quilts.get_quilt(quilt_id) do
      render(conn, "invitation_guest.html",
        changeset: changeset,
        callback_url: callback_url,
        quilt_id: quilt_id,
        skip_to_next_step_url: skip_to_next_step_url,
        social_share_url: social_share_url,
        name_for_quilt: name_for_quilt
      )
    else
      _ -> conn |> send_resp(404, "Not Found")
    end
  end

  # View Quilt

  def view_quilt(%{method: "GET"} = conn, %{"id" => quilt_id}) do
    {:ok, quilt} = QM.Quilts.get_quilt(quilt_id)
    {:ok, words} = QM.Quilts.get_word_counts(quilt_id)
    {:ok, memories} = QM.Quilts.get_text_memories(quilt_id)

    quilt_photo_url =
      case sign_quilt_photo(quilt) do
        {:ok, nil} -> nil
        {:ok, url} -> url
        {:error, _} -> nil
      end

    is_owner = get_session(conn, :user_id) == quilt.user_id
    is_authed = get_session(conn, :user_id) != nil

    render(conn, "view_quilt.html",
      quilt_subject: quilt.name,
      quilt_photo_url: quilt_photo_url,
      words: words,
      memories: memories,
      add_memory_url: Routes.quilt_path(conn, :add_memory_wizard_get, quilt_id),
      add_invitation_url: Routes.quilt_path(conn, :add_invitation_wizard_get, quilt_id),
      is_owner: is_owner,
      is_authed: is_authed
    )
  end

  defp sign_quilt_photo(%QM.Schemas.Quilt{photo_url: nil}) do
    {:ok, nil}
  end

  defp sign_quilt_photo(%QM.Schemas.Quilt{photo_url: full_photo_url}) do
    with get_config <- ExAws.Config.new(:s3),
         photo_url <- format_quilt_photo_url(full_photo_url),
         bucket <- Application.get_env(:quilted_memoir, :upload_bucket),
         {:ok, url} <-
           ExAws.S3.presigned_url(get_config, :get, bucket, photo_url, expires_in: 60) do
      {:ok, url}
    else
      _ -> {:error, :error_signing_quilt_photo_url}
    end
  end

  defp format_quilt_photo_url(photo_url) do
    [^photo_url, key] =
      Regex.run(
        ~r/https:\/\/.+\.amazonaws\.com\/quiltedmemoir(\/.*)/,
        photo_url
      )

    key
  end
end
