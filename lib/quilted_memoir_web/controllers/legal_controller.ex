defmodule QMWeb.LegalController do
  use QMWeb, :controller

  def terms_of_service(conn, _params) do
    render(conn, "terms_of_service.html")
  end
end
