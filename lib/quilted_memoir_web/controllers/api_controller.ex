defmodule QMWeb.ApiController do
  use QMWeb, :controller

  @bucket Application.get_env(:quilted_memoir, :upload_bucket)
  @expires 120

  def files(%{method: "POST"} = conn, _params) do
    # Verify user
    with {:ok, user_id} <- session_user(conn),
         file <- UUID.uuid4(),
         url_path <- "/quilt/u/#{user_id}/#{file}",
         put_config <- ExAws.Config.new(:s3),
         # Verify we don't need to put in %{content_type: foobar} here
         #  put_config <- Map.merge(base_config),
         {:ok, put_url} <-
           ExAws.S3.presigned_url(put_config, :put, @bucket, url_path, expires_in: @expires) do
      {:ok, %{status: :ok, url: put_url}}
      # If they are, sign an S3 PUT URL scoped to the user
    else
      {:error, err} -> {:error, err}
    end
    |> format_response(conn)
  end

  def quilt_photo(%{method: "POST"} = conn, %{"id" => quilt_id, "url" => photo_url} = _params) do
    with true <-
           valid_photo_url?(photo_url),
         {:ok, _quilt} <- QM.Quilts.set_quilt_photo_url(quilt_id, photo_url) do
      {:ok, %{status: :ok}}
    else
      {:error, err} -> {:error, err}
    end
    |> format_response(conn)
  end

  defp valid_photo_url?(url) do
    %{host: host} = ExAws.Config.new(:s3)

    case url |> String.starts_with?("https://#{host}/#{@bucket}/quilt/u/") do
      true -> true
      _ -> {:error, :invalid_quilt_photo_url}
    end
  end

  defp format_response({_, response} = full_response, conn) do
    with status <- status_from_response(full_response) do
      conn
      |> put_status(status)
      |> json(response)
    end
  end

  defp status_from_response(response) do
    case response do
      {:ok, _} -> 200
      {:error, %{reason: :not_logged_in, status: :error}} -> 403
      {:error, _} -> 400
    end
  end

  defp session_user(conn) do
    id = fetch_session(conn) |> get_session(:user_id)

    case id do
      nil -> {:error, %{status: :error, reason: :not_logged_in}}
      _ -> {:ok, id}
    end
  end
end
