defmodule QMWeb.AuthController do
  defmodule SignInForm do
    import Ecto.Changeset

    defstruct [:email, :password]

    @types %{email: :string, password: :string}

    def base_changeset(email \\ "", password \\ "") do
      {%__MODULE__{}, @types}
      |> cast(%{email: email, password: password}, Map.keys(@types))
    end

    def add_email_error(changeset, %{email: [message | _tail]}) do
      changeset
      |> add_error(:email, message)
    end

    def add_email_error(changeset, _) do
      changeset
    end

    def add_password_error(changeset, %{password: [message | _tail]}) do
      changeset
      |> add_error(:password, message)
    end

    def add_password_error(changeset, _) do
      changeset
    end

    def apply_action_insert(changeset) do
      {:error, new_changeset} = apply_action(changeset, :insert)
      new_changeset
    end
  end

  use QMWeb, :controller
  plug Ueberauth

  alias QM.Accounts
  alias Ueberauth.Strategy.Helpers

  def request(conn, %{"provider" => "identity"} = params) do
    callback_url = Helpers.callback_url(conn)
    email = Map.get(params, "email", "")
    changeset = SignInForm.base_changeset(email)
    render(conn, "sign_in.html", changeset: changeset, callback_url: callback_url)
  end

  def logout(conn, _params) do
    conn
    |> put_flash(:info, "You have logged out")
    |> delete_session(:user_id)
    |> redirect(to: "/")
  end

  # Ueberauth callbacks

  # Username/Password (Identity)

  def identity_callback(
        %{assigns: %{ueberauth_auth: %{provider: :identity} = auth}} = conn,
        _params
      ) do
    case Accounts.login(auth.uid, auth.credentials.other.password) do
      {:ok, account} ->
        conn
        |> put_flash(:info, "You are now logged in.")
        |> put_session(:user_id, account.id)
        |> redirect(to: "/")

      {:error, reason} ->
        flash =
          reason
          |> Phoenix.Naming.humanize()

        conn
        |> put_flash(:error, flash)
        |> redirect(to: "/auth/identity")
    end
  end
end
