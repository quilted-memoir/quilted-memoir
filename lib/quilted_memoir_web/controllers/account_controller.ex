defmodule QMWeb.AccountController do
  defmodule SignUpForm do
    import Ecto.Changeset

    defstruct [:email, :password]

    @types %{email: :string, password: :string, password_again: :string}

    def base_changeset(email \\ "", password \\ "") do
      {%__MODULE__{}, @types}
      |> cast(%{email: email, password: password}, Map.keys(@types))
    end

    def add_email_error(changeset, %{email: [message | _tail]}) do
      changeset
      |> add_error(:email, message)
    end

    def add_email_error(changeset, _) do
      changeset
    end

    def add_password_error(changeset, %{password: [message | _tail]}) do
      changeset
      |> add_error(:password, message)
    end

    def add_password_error(changeset, :passwords_must_match) do
      changeset
      |> add_error(:password, "Passwords must match")
    end

    def add_password_error(changeset, _) do
      changeset
    end

    def apply_action_insert(changeset) do
      {:error, new_changeset} = apply_action(changeset, :insert)
      new_changeset
    end
  end

  defmodule ResetPasswordForm do
    import Ecto.Changeset

    defstruct [:email]

    @types %{email: :string}

    def base_changeset(email \\ "") do
      {%__MODULE__{}, @types}
      |> cast(%{email: email}, Map.keys(@types))
    end

    def add_email_error(changeset, %{email: [message | _tail]}) do
      changeset
      |> add_error(:email, message)
    end

    def add_email_error(changeset, _) do
      changeset
    end

    def apply_action_insert(changeset) do
      {:error, new_changeset} = apply_action(changeset, :insert)
      new_changeset
    end
  end

  defmodule FinishPasswordResetForm do
    import Ecto.Changeset

    defstruct [:token, :password, :password_verify]

    @types %{token: :string, password: :string, password_verify: :string}

    def base_changeset(token \\ "", password \\ "", password_verify \\ "") do
      {%__MODULE__{}, @types}
      |> cast(
        %{token: token, password: password, password_verify: password_verify},
        Map.keys(@types)
      )
    end

    def add_token_error(changeset, %{token: [message | _tail]}) do
      changeset
      |> add_error(:token, message)
    end

    def add_token_error(changeset, _) do
      changeset
    end

    def add_password_error(changeset, %{password: [message | _tail]}) do
      changeset
      |> add_error(:password, message)
    end

    def add_password_error(changeset, :passwords_must_match) do
      changeset
      |> add_error(:password, "Passwords must match")
    end

    def add_password_error(changeset, _) do
      changeset
    end

    def apply_action_insert(changeset) do
      {:error, new_changeset} = apply_action(changeset, :insert)
      new_changeset
    end
  end

  defmodule RegisterGuestForm do
    import Ecto.Changeset

    defstruct [:email]

    @types %{email: :string}

    def base_changeset(email \\ "") do
      {%__MODULE__{}, @types}
      |> cast(%{email: email}, Map.keys(@types))
    end

    def add_email_error(changeset, %{email: [message | _tail]}) do
      changeset
      |> add_error(:email, message)
    end

    def add_email_error(changeset, _) do
      changeset
    end

    def apply_action_insert(changeset) do
      {:error, new_changeset} = apply_action(changeset, :insert)
      new_changeset
    end
  end

  use QMWeb, :controller

  alias QM.Accounts

  def register_get(%{method: "GET"} = conn, _params) do
    changeset = SignUpForm.base_changeset()
    render(conn, "register.html", changeset: changeset)
  end

  def register_post(
        %{method: "POST"} = conn,
        %{
          "sign_up_form" => %{
            "email" => email,
            "password" => password,
            "password_verify" => password_verify
          }
        }
      ) do
    email
    |> Accounts.register_account(password, password_verify)
    |> handle_signup_result(conn, email)
  end

  def register_post(%{method: "POST"} = conn, _params) do
    changeset = SignUpForm.base_changeset()
    render(conn, "register.html", changeset: changeset)
  end

  def register_guest_get(%{method: "GET"} = conn, %{"quilt_id" => quilt_id}) do
    changeset = RegisterGuestForm.base_changeset()

    {:ok, %{name: quilt_subject}} = QM.Quilts.get_quilt(quilt_id)

    conn
    |> put_session(:return_path_quilt_id, quilt_id)
    |> render("register_guest.html", changeset: changeset, quilt_subject: quilt_subject)
  end

  def register_guest_post(%{method: "POST"} = conn, %{
        "register_guest_form" => %{"email" => email}
      }) do
    with {:ok, %QM.Schemas.Account{id: guest_id}} <- Accounts.register_guest(email),
         return_quilt_id <- get_session(conn, :return_path_quilt_id) do
      redirect(conn, to: Routes.quilt_path(conn, :add_memory_guest_get, return_quilt_id, guest_id))
    end
  end

  defp handle_signup_result({:ok, account}, conn, _email) do
    conn
    |> put_flash(:info, "You are now logged in.")
    |> put_session(:user_id, account.id)
    |> redirect(to: "/")
  end

  defp handle_signup_result({:error, err}, conn, email) do
    changeset =
      email
      |> SignUpForm.base_changeset()
      |> SignUpForm.add_email_error(err)
      |> SignUpForm.add_password_error(err)
      |> SignUpForm.apply_action_insert()

    conn
    |> render("register.html", changeset: changeset)
  end

  def reset_password_get(%{method: "GET"} = conn, _params) do
    changeset = ResetPasswordForm.base_changeset()
    render(conn, "reset_password.html", changeset: changeset)
  end

  def reset_password_post(%{method: "POST"} = conn, %{
        "reset_password_form" => %{"email" => email}
      }) do
    Accounts.request_password_reset(email)

    conn
    |> put_flash(:info, "Password reset requested. Please check your email.")
    |> redirect(to: "/")
  end

  def finish_password_reset_get(%{method: "GET"} = conn, %{"token" => token}) do
    changeset = FinishPasswordResetForm.base_changeset(token)
    render(conn, "finish_reset_password.html", changeset: changeset)
  end

  def finish_password_reset_get(%{method: "GET"} = conn, _params) do
    changeset = FinishPasswordResetForm.base_changeset()
    render(conn, "finish_reset_password.html", changeset: changeset)
  end

  def finish_password_reset_post(%{method: "POST"} = conn, %{
        "finish_password_reset_form" => %{
          "token" => token,
          "password" => password,
          "password_verify" => password_verify
        }
      })
      when password == password_verify do
    case Accounts.reset_password(token, password) do
      :ok ->
        conn
        |> put_flash(:info, "Password reset successfully. You may now login.")
        |> redirect(to: "/auth/identity")

      {:error, error} ->
        with changeset <-
               FinishPasswordResetForm.base_changeset(token)
               |> FinishPasswordResetForm.add_password_error(error)
               |> FinishPasswordResetForm.apply_action_insert() do
          conn
          |> put_flash(:error, "Error resetting password")
          |> render("finish_reset_password.html", changeset: changeset)
        end
    end
  end

  def finish_password_reset_post(%{method: "POST"} = conn, %{
        "finish_password_reset_form" => %{
          "token" => token,
          "password" => password,
          "password_verify" => password_verify
        }
      })
      when password != password_verify do
    changeset = FinishPasswordResetForm.base_changeset(token)

    conn
    |> put_flash(:error, "Passwords must match")
    |> render("finish_reset_password.html", changeset: changeset)
  end
end
