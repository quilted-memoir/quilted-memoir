defmodule QMWeb.Router do
  use QMWeb, :router
  use Plug.ErrorHandler
  use Sentry.Plug

  import Plug.BasicAuth
  import Phoenix.LiveDashboard.Router

  # Get custom_headers working
  # @custom_headers %{"content-security-policy" => ""}
  @custom_headers %{}

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers, @custom_headers
    plug :assign_user_from_session
  end

  pipeline :api do
    plug :accepts, ["json"]
    plug :fetch_session
    plug :assign_user_from_session
  end

  pipeline :admins_only do
    plug :basic_auth,
      username: "admin",
      password: "PefViGk7XjM.8YP.MGK7iY3ys-nHGn9V9pFAMf*Yjfam*egxU"
  end

  scope "/" do
    pipe_through [:browser, :admins_only]
    live_dashboard "/dashboard", metrics: QMWeb.Telemetry
  end

  scope "/", QMWeb do
    pipe_through :browser

    get "/", PageController, :index
    get "/about", PageController, :about
  end

  scope "/account", QMWeb do
    pipe_through :browser

    get "/register", AccountController, :register_get
    post "/register", AccountController, :register_post

    get "/reset-password", AccountController, :reset_password_get
    post "/reset-password", AccountController, :reset_password_post

    get "/reset-password/finish", AccountController, :finish_password_reset_get
    post "/reset-password/finish", AccountController, :finish_password_reset_post

    get "/register-guest", AccountController, :register_guest_get
    post "/register-guest", AccountController, :register_guest_post
  end

  scope "/auth", QMWeb do
    pipe_through :browser

    get "/login", PageController, :index

    # Ueberauth routes
    get "/logout", AuthController, :logout
    get "/:provider", AuthController, :request
    get "/:provider/callback", AuthController, :callback
    post "/identity/callback", AuthController, :identity_callback
  end

  scope "/quilt", QMWeb do
    pipe_through [:browser, :authentication_required]

    get "/", QuiltController, :index

    get "/new", QuiltController, :new
    post "/new", QuiltController, :new_post

    get "/:id/wizard/add-memory", QuiltController, :add_memory_wizard_get
    post "/:id/wizard/add-memory", QuiltController, :add_memory_wizard_post

    get "/:id/wizard/add-invitation", QuiltController, :add_invitation_wizard_get
    post "/:id/wizard/add-invitation", QuiltController, :add_invitation_wizard_post

    get "/:id/wizard/set-photo", QuiltController, :set_quilt_photo_wizard_get
    post "/:id/wizard/set-photo", QuiltController, :set_quilt_photo_wizard_post
  end

  scope "/quilt", QMWeb do
    pipe_through :browser

    # If a guest doesn't have an ID, then we ask them for an email and register them, then send them on their way
    get "/:id/guest/add-memory", QuiltController, :add_memory_guest

    get "/:id/guest/:guest_id/add-memory", QuiltController, :add_memory_guest_get
    post "/:id/guest/:guest_id/add-memory", QuiltController, :add_memory_guest_post

    get "/:id/guest/:guest_id/add-invitation", QuiltController, :add_invitation_guest_get
    post "/:id/guest/:guest_id/add-invitation", QuiltController, :add_invitation_guest_post

    get "/:id", QuiltController, :view_quilt
  end

  scope "/legal", QMWeb do
    pipe_through :browser

    get "/terms-of-service", LegalController, :terms_of_service
  end

  if Mix.env() == :dev do
    # If using Phoenix
    forward "/sent_emails", Bamboo.SentEmailViewerPlug
  end

  # Other scopes may use custom stacks.
  # scope "/api", QMWeb do
  #   pipe_through :api
  # end

  scope "/api", QMWeb do
    pipe_through :api

    post "/files", ApiController, :files
    post "/quilt/:id/photo", ApiController, :quilt_photo
  end

  defp authentication_required(conn, _) do
    case get_session(conn, :user_id) do
      nil ->
        conn
        |> Phoenix.Controller.put_flash(:error, "Please login")
        |> Phoenix.Controller.redirect(to: "/auth/identity")
        |> halt()

      _ ->
        conn
    end
  end

  defp assign_user_from_session(conn, _) do
    case get_session(conn, :user_id) do
      nil ->
        conn

      user_id ->
        assign(conn, :current_user, QM.Accounts.get_account(user_id))
    end
  end
end
