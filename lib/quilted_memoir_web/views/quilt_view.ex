defmodule QMWeb.QuiltView do
  use QMWeb, :view

  def quilt_link(quilt_id), do: "/quilt/#{quilt_id |> UUID.binary_to_string!()}"

  def linear_gradient_stops do
    gradient_options = [
      {"#0AB6F8", "#ED2D53"},
      {"#F4D941", "#EC8235"},
      {"#30C5D2", "#471069"},
      {"#F756AA", "#F75672"},
      {"#1ED7B5", "#F0F9A7"},
      {"#CB4288", "#B6353B"},
      {"#FFD78A", "#F4762D"},
      {"#82F4B1", "#30C67C"},
      {"#43C197", "#1C1554"},
      {"#2FEAA8", "#028CF3"}
    ]

    {start, stop} = Enum.random(gradient_options)

    {:safe,
     """
     <stop offset="0" stop-color="#{start}" />
     <stop offset="1" stop-color="#{stop}" />
     """}
  end
end
