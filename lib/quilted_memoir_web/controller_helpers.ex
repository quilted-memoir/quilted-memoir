defmodule QMWeb.ControllerHelpers do
  @moduledoc """
  Multi-purpose functions for controllers
  """

  def current_user_id(conn) do
    with id <- Plug.Conn.get_session(conn, "user_id"), false <- is_nil(id) do
      id
    else
      _ -> {:error, :anonymous_session}
    end
  end
end
