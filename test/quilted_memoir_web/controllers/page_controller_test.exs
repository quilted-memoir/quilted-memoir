defmodule QMWeb.PageControllerTest do
  use QMWeb.ConnCase

  test "GET /", %{conn: conn} do
    conn = get(conn, "/")
    assert html_response(conn, 200) =~ "Get The Whole Story"
  end
end
