defmodule QMWeb.AuthControllerTest do
  use QMWeb.ConnCase

  alias QM.Accounts
  alias QMES.Events.AccountRegistered

  @path_prefix "/auth"

  test "GET /identity", %{conn: conn} do
    conn = get(conn, path("/identity"))
    assert html_response(conn, 200) =~ "Password"
  end

  test "POST /identity/callback", %{conn: conn} do
    email = "foo@bar.com"
    password = "Password must be something long!"

    {:ok, %{id: account_id}} = Accounts.register_account(email, password, password)

    wait_for_event(QMES.Application, AccountRegistered, fn event -> event.email == email end)

    conn =
      post(conn, path("/identity/callback"), %{
        "sign_in_form" => %{
          "email" => email,
          "password" => password
        }
      })

    assert redirected_to(conn, 302) =~ "/"

    assert get_flash(conn, "info") =~ "You are now logged in."

    assert %{private: %{:plug_session => %{"user_id" => ^account_id}}} = conn
  end

  test "Invalid POST /identity/callback", %{conn: conn} do
    email = "foo@bar.com"
    password = "Password must be something long!"

    conn =
      post(conn, path("/identity/callback"), %{
        "sign_in_form" => %{
          "email" => email,
          "password" => password
        }
      })

    # assert redirected_to(conn, 302) =~ "/"

    assert get_flash(conn, "error") =~ "Invalid username or password"

    %{private: %{:plug_session => session}} = conn
    assert Map.get(session, "current_user") |> is_nil()
  end

  defp path(suffix) do
    @path_prefix <> suffix
  end
end
