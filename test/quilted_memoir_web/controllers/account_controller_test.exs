defmodule QMWeb.AccountControllerTest do
  use QMWeb.ConnCase

  alias QM.Accounts
  alias QMES.Events.AccountRegistered

  @path_prefix "/account"

  test "GET /register", %{conn: conn} do
    conn = get(conn, path("/register"))
    assert html_response(conn, 200) =~ "Password Again"
  end

  test "POST /register", %{conn: conn} do
    conn =
      post(conn, path("/register"), %{
        "sign_up_form" => %{
          "email" => "foo@bar.com",
          "password" => "Password must be something long!",
          "password_verify" => "Password must be something long!"
        }
      })

    assert redirected_to(conn, 302) =~ "/"

    assert get_flash(conn, "info") =~ "You are now logged in."

    wait_for_event(QMES.Application, AccountRegistered)

    assert {:ok, %{id: account_id}} = Accounts.get_account_by_email("foo@bar.com")

    assert %{private: %{:plug_session => %{"user_id" => ^account_id}}} = conn
  end

  test "POST /register with short password", %{conn: conn} do
    conn =
      post(conn, path("/register"), %{
        "sign_up_form" => %{
          "email" => "foo@bar.com",
          "password" => "Too short",
          "password_verify" => "Too short"
        }
      })

    assert html_response(conn, 200) =~ "String is too short. Only 9 characters instead of 14"
  end

  defp path(suffix) do
    @path_prefix <> suffix
  end
end
