defmodule QMWeb.QuiltControllerTest do
  use QMWeb.ConnCase

  alias QM.Quilts

  alias QMES.Events.QuiltStarted

  @path_prefix "/quilt"

  test "GET /new offers to start a new quilt", %{conn: conn} do
    conn = AccountHelpers.start_session(conn) |> get(path("/new"))
    assert html_response(conn, 200) =~ "New Quilt"
  end

  test "POST /new starts a new quilt", %{conn: conn} do
    name = Faker.Name.name()

    conn =
      AccountHelpers.start_session(conn)
      |> post(
        path("/new"),
        %{
          "new_quilt_form" => %{
            "name" => name
          }
        }
      )

    wait_for_event(QMES.Application, QuiltStarted)

    user_id = get_session(conn, "user_id")

    {:ok, [%QM.Schemas.Quilt{id: quilt_uuid, name: ^name}]} = Quilts.get_quilts(user_id)
    quilt_id = UUID.binary_to_string!(quilt_uuid)

    assert get_flash(conn, "success") =~ "Quilt Opened"

    assert redirected_to(conn, 303) =~ "/quilt/#{quilt_id}/wizard/add-memory"
  end

  test "POST /new with no name fails", %{conn: conn} do
    name = ""

    conn =
      AccountHelpers.start_session(conn)
      |> post(
        path("/new"),
        %{
          "new_quilt_form" => %{
            "name" => name
          }
        }
      )

    user_id = get_session(conn, "user_id")

    assert {:ok, []} = Quilts.get_quilts(user_id)

    assert get_flash(conn, "error") =~ "You must set a name for this quilt"

    assert html_response(conn, 200) =~ "New Quilt"
  end

  defp path(suffix) do
    @path_prefix <> suffix
  end
end
