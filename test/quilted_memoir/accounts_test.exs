defmodule QM.AccountsTests do
  use QM.EventCase
  use Bamboo.Test, shared: true

  alias QMES.Events.{
    AccountRegistered,
    PasswordResetRequested,
    PasswordResetTokenExpired,
    PasswordResetWithToken
  }

  alias QM.Schemas.Account

  describe "Account Registration" do
    test "can register an account with an email and password" do
      {email, password, _} = AccountHelpers.generate_account()

      {:ok, %Account{email: registered_email}} =
        QM.Accounts.register_account(email, password, password)

      assert email == registered_email
    end

    test "fails when passwords don't match" do
      {email, password, password_again} = AccountHelpers.generate_account()

      {:error, :passwords_must_match} =
        QM.Accounts.register_account(email, password, password_again)
    end

    test "fails when an email has been registered twice" do
      {email, password, _} = AccountHelpers.generate_account()
      {:ok, _} = QM.Accounts.register_account(email, password, password)

      {:error, %{email: ["Email address already taken"]}} =
        QM.Accounts.register_account(email, password, password)
    end
  end

  describe "Account login" do
    test "can login to an account with the correct email and password" do
      {email, password, _} = AccountHelpers.generate_account()
      {:ok, _} = QM.Accounts.register_account(email, password, password)
      wait_for_event(QMES.Application, AccountRegistered, fn event -> event.email == email end)
      {:ok, %Account{} = account} = QM.Accounts.login(email, password)
      assert account.email == email
    end

    test "get an error when using an unknown email" do
      {email, password, _} = AccountHelpers.generate_account()
      {:error, :invalid_username_or_password} = QM.Accounts.login(email, password)
    end

    test "get an error when using a valid email with a bad password" do
      {email, password, different_password} = AccountHelpers.generate_account()
      {:ok, _} = QM.Accounts.register_account(email, password, password)
      {:error, :invalid_username_or_password} = QM.Accounts.login(email, different_password)
    end

    test "generate the same error for both unknown email and bad passwords" do
      {email, password, different_password} = AccountHelpers.generate_account()
      {:ok, _} = QM.Accounts.register_account(email, password, password)
      {:error, login_with_bad_password_error} = QM.Accounts.login(email, different_password)

      {unknown_email, another_password, _} = AccountHelpers.generate_account()
      {:error, login_with_bad_email_error} = QM.Accounts.login(unknown_email, another_password)

      assert login_with_bad_password_error == login_with_bad_email_error
    end
  end

  describe "password reset" do
    test "password reset requested event on valid account" do
      {email, password, _} = AccountHelpers.generate_account()
      {:ok, account} = QM.Accounts.register_account(email, password, password)

      wait_for_event(QMES.Application, AccountRegistered, fn event -> event.email == email end)

      {:ok, _reset_token, _expiration} = QM.Accounts.request_password_reset(email)

      assert_receive_event(QMES.Application, QMES.Events.PasswordResetRequested, fn event ->
        assert event.account_id == account.id
        assert is_bitstring(event.password_reset_token)
      end)
    end

    test "token generated even if the account doesn't exist" do
      {email, _, _} = AccountHelpers.generate_account()

      {:ok, reset_token, _expiration} = QM.Accounts.request_password_reset(email)
      assert is_bitstring(reset_token)
    end

    test "password reset requested valid for 15 minutes" do
      {email, password, _} = AccountHelpers.generate_account()
      {:ok, _account} = QM.Accounts.register_account(email, password, password)
      {:ok, _reset_token, expiration_stamp} = QM.Accounts.request_password_reset(email)

      {:ok, expiration} = Timex.parse(expiration_stamp, "{ISO:Extended:Z}")
      time_diff = Timex.diff(expiration, Timex.now(), :seconds)
      fifteen_minutes_in_seconds = 900

      # Drop 1 second as we round down from microsecond latency in the system
      assert time_diff == fifteen_minutes_in_seconds - 1
    end

    @tag :only
    test "password is reset when we use a valid reset token" do
      {email, password, new_password} = AccountHelpers.generate_account()
      {:ok, _account} = QM.Accounts.register_account(email, password, password)
      wait_for_event(QMES.Application, AccountRegistered, fn event -> event.email == email end)
      Process.sleep(250)
      {:ok, %Account{} = account_one} = QM.Accounts.login(email, password)

      {:ok, reset_token, _expiration} = QM.Accounts.request_password_reset(email)

      wait_for_event(QMES.Application, PasswordResetRequested, fn event ->
        event.account_id == account_one.id
      end)

      :ok = QM.Accounts.reset_password(reset_token, new_password)

      wait_for_event(QMES.Application, PasswordResetWithToken, fn event ->
        event.password_reset_token == reset_token
      end)

      {:ok, %Account{} = account_two} = QM.Accounts.login(email, new_password)
      assert account_one.id == account_two.id
      assert account_one.email == account_two.email
      assert account_one.email == email
      assert account_one.password_hashed != account_two.password_hashed
    end

    test "password stays the same with an invalid reset token" do
      {email, password, new_password} = AccountHelpers.generate_account()
      {:ok, _account} = QM.Accounts.register_account(email, password, password)
      wait_for_event(QMES.Application, AccountRegistered, fn event -> event.email == email end)
      {:ok, %Account{} = account_one} = QM.Accounts.login(email, password)

      {:error, :account_not_found} = QM.Accounts.reset_password("fake token", new_password)
      {:ok, %Account{} = account_two} = QM.Accounts.login(email, password)
      assert account_one.id == account_two.id
      assert account_one.email == account_two.email
      assert account_one.email == email
      assert account_one.password_hashed == account_two.password_hashed
    end

    test "password reset token becomes invalid after expiration period" do
      {email, password, new_password} = AccountHelpers.generate_account()
      {:ok, _account} = QM.Accounts.register_account(email, password, password)

      wait_for_event(QMES.Application, AccountRegistered, fn event -> event.email == email end)
      Process.sleep(50)

      {:ok, %Account{}} = QM.Accounts.login(email, password)

      {:ok, token, _expiration} = QM.Accounts.request_password_reset(email)

      assert %{success: 1, failure: 0} == Oban.drain_queue(:commanded, with_scheduled: true)

      wait_for_event(QMES.Application, PasswordResetTokenExpired)

      # Wait for projections to update
      Process.sleep(50)
      {:error, :account_not_found} = QM.Accounts.reset_password(token, new_password)
      {:error, :invalid_username_or_password} = QM.Accounts.login(email, new_password)
    end

    test "password reset email dispatched when token requested" do
      {email, password, _new_password} = AccountHelpers.generate_account()
      {:ok, _account} = QM.Accounts.register_account(email, password, password)
      wait_for_event(QMES.Application, AccountRegistered)
      {:ok, %Account{email: email}} = QM.Accounts.login(email, password)

      {:ok, reset_token, expiration} = QM.Accounts.request_password_reset(email)
      wait_for_event(QMES.Application, PasswordResetRequested)
      assert_delivered_email(QM.Email.password_reset_requested(email, reset_token, expiration))
    end
  end
end
