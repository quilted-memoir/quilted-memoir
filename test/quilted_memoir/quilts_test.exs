defmodule QM.QuiltsTest do
  use QM.EventCase
  use Bamboo.Test, shared: true

  alias QMES.Events.{
    AccountRegistered,
    ContributorInvited,
    GuestRegistered,
    QuiltStarted,
    TextMemoryAdded
  }

  alias QM.Schemas.{Quilt, TextMemory}

  test "start_quilt" do
    user_id = Faker.UUID.v4()

    {:ok, %Quilt{user_id: fetched_user_id}} = QM.Quilts.start_quilt(user_id, "This is for dad")

    assert user_id == fetched_user_id
  end

  test "get_quilts" do
    user_id = Faker.UUID.v4()

    QM.Quilts.start_quilt(user_id, "This is for dad")

    wait_for_event(QMES.Application, QuiltStarted, fn event -> event.user_id == user_id end)

    {:ok, [%QM.Schemas.Quilt{id: _id, name: _name, user_id: user_id}]} =
      QM.Quilts.get_quilts(user_id)

    assert user_id === user_id
  end

  test "add_text_memory" do
    user_id = Faker.UUID.v4()
    memory = Faker.StarWars.quote()

    {:ok, %{id: quilt_id}} = QM.Quilts.start_quilt(user_id, "This is for dad")
    wait_for_event(QMES.Application, QuiltStarted, fn event -> event.user_id == user_id end)
    added_memory = QM.Quilts.add_text_memory(quilt_id, user_id, memory)

    assert {:ok, %TextMemory{id: _id, quilt_id: quilt_id, user_id: user_id, memory: memory}} =
             added_memory
  end

  test "add_text_memory generates counts" do
    user_id = Faker.UUID.v4()
    memory = Faker.StarWars.quote()

    {:ok, %{id: quilt_id}} = QM.Quilts.start_quilt(user_id, "This is for dad")
    wait_for_event(QMES.Application, QuiltStarted, fn event -> event.user_id == user_id end)
    _added_memory = QM.Quilts.add_text_memory(quilt_id, user_id, memory)
    wait_for_event(QMES.Application, TextMemoryAdded, fn event -> event.quilt_id == quilt_id end)

    {:ok, words} = QM.Quilts.get_word_counts(quilt_id)

    assert length(words) > 0
  end

  test "add_text_memory upserts counts" do
    user_id = Faker.UUID.v4()
    memory = Faker.StarWars.quote()

    {:ok, %{id: quilt_id}} = QM.Quilts.start_quilt(user_id, "This is for dad")
    wait_for_event(QMES.Application, QuiltStarted, fn event -> event.user_id == user_id end)

    QM.Quilts.add_text_memory(quilt_id, user_id, memory)
    wait_for_event(QMES.Application, TextMemoryAdded, fn event -> event.quilt_id == quilt_id end)
    {:ok, first_words} = QM.Quilts.get_word_counts(quilt_id)

    QM.Quilts.add_text_memory(quilt_id, user_id, memory)
    wait_for_event(QMES.Application, TextMemoryAdded, fn event -> event.quilt_id == quilt_id end)
    {:ok, new_words} = QM.Quilts.get_word_counts(quilt_id)

    doubled_word_counts =
      first_words |> Enum.map(fn %{count: count} -> count end) |> Enum.map(fn x -> x * 2 end)

    new_word_counts = new_words |> Enum.map(fn %{count: count} -> count end)

    assert doubled_word_counts == new_word_counts
  end

  test "get_text_memory" do
    user_id = Faker.UUID.v4()

    {:ok, %{id: quilt_id}} = QM.Quilts.start_quilt(user_id, "This is for dad")

    wait_for_event(QMES.Application, QuiltStarted, fn event -> event.user_id == user_id end)

    {:ok, %{id: memory_id}} = QM.Quilts.add_text_memory(quilt_id, user_id, "I remember when...")

    wait_for_event(QMES.Application, TextMemoryAdded, fn event ->
      event.user_id == user_id
    end)

    {:ok, %{memory: memory}} = QM.Quilts.get_text_memory(memory_id)
    assert memory === "I remember when..."
  end

  test "invite_contributor" do
    {inviting_email, password, _} = AccountHelpers.generate_account()

    {:ok, %{id: inviting_user_id}} =
      QM.Accounts.register_account(inviting_email, password, password)

    wait_for_event(QMES.Application, AccountRegistered, fn event ->
      event.account_id == inviting_user_id
    end)

    {:ok, %{id: quilt_id}} = QM.Quilts.start_quilt(inviting_user_id, "This is for dad")

    wait_for_event(QMES.Application, QuiltStarted, fn event ->
      event.user_id == inviting_user_id
    end)

    invited_email = Faker.Internet.email()

    :ok = QM.Quilts.invite_contributor(quilt_id, inviting_user_id, invited_email)

    wait_for_event(QMES.Application, ContributorInvited, fn event ->
      event.quilt_id == quilt_id
    end)
  end

  test "invite_contributor on existing contributor sends an email" do
    {inviting_email, password, _} = AccountHelpers.generate_account()

    {:ok, %{id: inviting_user_id}} =
      QM.Accounts.register_account(inviting_email, password, password)

    wait_for_event(QMES.Application, AccountRegistered, fn event ->
      event.account_id == inviting_user_id
    end)

    quilt_subject = "Brian Anderson"

    {:ok, %{id: quilt_id}} = QM.Quilts.start_quilt(inviting_user_id, quilt_subject)

    wait_for_event(QMES.Application, QuiltStarted, fn event ->
      event.user_id == inviting_user_id
    end)

    invited_email = Faker.Internet.email()

    :ok = QM.Quilts.invite_contributor(quilt_id, inviting_user_id, invited_email)

    wait_for_event(QMES.Application, ContributorInvited, fn event ->
      event.quilt_id == quilt_id
    end)

    {:ok, %{id: invited_user_id}} = QM.Accounts.get_account_by_email(invited_email)

    another_quilt_subject = "Brent Anderson"

    {:ok, %{id: another_quilt_id}} =
      QM.Quilts.start_quilt(inviting_user_id, another_quilt_subject)

    wait_for_event(QMES.Application, QuiltStarted, fn event ->
      event.user_id == inviting_user_id
    end)

    :ok = QM.Quilts.invite_contributor(another_quilt_id, inviting_user_id, invited_email)

    wait_for_event(QMES.Application, ContributorInvited, fn event ->
      event.quilt_id == another_quilt_id
    end)

    {:ok, %{id: invited_user_id_again}} = QM.Accounts.get_account_by_email(invited_email)

    assert invited_user_id_again == invited_user_id

    expected_email =
      QM.Email.contributor_invitation(
        another_quilt_id,
        inviting_user_id,
        invited_user_id,
        another_quilt_subject
      )

    assert_delivered_email(expected_email)
  end

  test "invite_contributor sends an email" do
    {inviting_email, password, _} = AccountHelpers.generate_account()

    {:ok, %{id: inviting_user_id}} =
      QM.Accounts.register_account(inviting_email, password, password)

    wait_for_event(QMES.Application, AccountRegistered, fn event ->
      event.account_id == inviting_user_id
    end)

    quilt_subject = "Brian Anderson"

    {:ok, %{id: quilt_id}} = QM.Quilts.start_quilt(inviting_user_id, quilt_subject)

    wait_for_event(QMES.Application, QuiltStarted, fn event ->
      event.user_id == inviting_user_id
    end)

    invited_email = Faker.Internet.email()

    :ok = QM.Quilts.invite_contributor(quilt_id, inviting_user_id, invited_email)

    wait_for_event(QMES.Application, ContributorInvited, fn event ->
      event.quilt_id == quilt_id
    end)

    {:ok, %{id: invited_user_id}} = QM.Accounts.get_account_by_email(invited_email)

    expected_email =
      QM.Email.contributor_invitation(
        quilt_id,
        inviting_user_id,
        invited_user_id,
        quilt_subject
      )

    assert_delivered_email(expected_email)
  end

  test "invite_contributor fails when the same email is invited twice" do
    {inviting_email, password, _} = AccountHelpers.generate_account()

    {:ok, %{id: inviting_user_id}} =
      QM.Accounts.register_account(inviting_email, password, password)

    wait_for_event(QMES.Application, AccountRegistered, fn event ->
      event.account_id == inviting_user_id
    end)

    {:ok, %{id: quilt_id}} = QM.Quilts.start_quilt(inviting_user_id, "This is for dad")

    wait_for_event(QMES.Application, QuiltStarted, fn event ->
      event.user_id == inviting_user_id
    end)

    invited_email = Faker.Internet.email()

    :ok = QM.Quilts.invite_contributor(quilt_id, inviting_user_id, invited_email)

    wait_for_event(QMES.Application, ContributorInvited, fn event ->
      event.quilt_id == quilt_id
    end)

    wait_for_event(QMES.Application, GuestRegistered, fn event ->
      event.email == invited_email
    end)

    {:error, :email_already_invited} =
      QM.Quilts.invite_contributor(quilt_id, inviting_user_id, invited_email)
  end
end
