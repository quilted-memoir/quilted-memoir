defmodule QMWeb.AccountHelpers do
  @moduledoc """
  Test helpers for creating fake accounts and logging into them
  """

  alias Faker.Lorem.Shakespeare
  alias QM.Schemas.Account
  alias QMES.Events.AccountRegistered

  import Commanded.Assertions.EventAssertions

  require Phoenix.ConnTest
  import Phoenix.ConnTest

  @endpoint QMWeb.Endpoint

  def generate_account do
    email = Faker.Internet.email()
    password = Shakespeare.as_you_like_it()
    different_password = Shakespeare.hamlet()
    {email, password, different_password}
  end

  def register_account(email, password) do
    {:ok, %Account{email: _registered_email}} =
      QM.Accounts.register_account(email, password, password)

    wait_for_event(QMES.Application, AccountRegistered, fn event -> event.email == email end)

    QM.Accounts.get_account_by_email(email)
  end

  def login_to_session(conn, email, password) do
    conn
    |> Phoenix.ConnTest.post("/auth/identity/callback", %{
      "sign_in_form" => %{
        "email" => email,
        "password" => password
      }
    })
    |> recycle()
  end

  def start_session(conn) do
    {email, password, _} = generate_account()
    {:ok, _} = register_account(email, password)
    conn |> login_to_session(email, password)
  end
end
