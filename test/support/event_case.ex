# test/support/data_case.ex
defmodule QM.EventCase do
  @moduledoc """
  Handle setup for event-sourcing centric test cases
  """

  use ExUnit.CaseTemplate

  using do
    quote do
      alias QMWeb.AccountHelpers

      import Ecto
      import Ecto.Changeset
      import Ecto.Query
      import Commanded.Assertions.EventAssertions
    end
  end

  setup do
    {:ok, _} = Application.ensure_all_started(:quilted_memoir)

    on_exit(fn ->
      :ok = Application.stop(:quilted_memoir)
      :ok = Application.stop(:commanded)

      QM.Storage.reset!()
    end)

    :ok
  end
end
