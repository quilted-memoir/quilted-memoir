defmodule QM.Storage do
  @moduledoc """
  Clear the event store and read store databases
  """

  alias EventStore.Storage.Initializer

  def reset! do
    reset_eventstore()
    reset_readstore()
  end

  defp reset_eventstore do
    config =
      QMES.EventStore.config()
      |> EventStore.Config.default_postgrex_opts()

    {:ok, conn} = Postgrex.start_link(config)

    Initializer.reset!(conn)
  end

  defp reset_readstore do
    config = Application.get_env(:quilted_memoir, QM.Repo)

    {:ok, conn} = Postgrex.start_link(config)

    Postgrex.query!(conn, truncate_readstore_tables(), [])
  end

  defp truncate_readstore_tables do
    """
    TRUNCATE TABLE
    accounts,
    quilts,
    projection_versions,
    oban_beats,
    oban_jobs
    RESTART IDENTITY
    CASCADE;
    """
  end
end
