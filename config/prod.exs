use Mix.Config

config :quilted_memoir,
  enable_sentry: true,
  namespace: QM,
  ecto_repos: [QM.Repo],
  generators: [binary_id: true]

config :quilted_memoir, QMWeb.Endpoint,
  url: [host: "www.quiltedmemoir.com", port: 443, scheme: "https"],
  http: [port: System.get_env("PORT") || 4000],
  secret_key_base: System.get_env("PHOENIX_SECRET_KEY_BASE"),
  cache_static_manifest: "priv/static/cache_manifest.json",
  check_origin: ["//quiltedmemoir.com"],
  server: true

config :logger, level: :info

config :quilted_memoir, QM.Repo,
  username: System.get_env("DATABASE_USER"),
  password: System.get_env("DATABASE_PASS"),
  database: System.get_env("DATABASE_NAME"),
  hostname: System.get_env("DATABASE_HOST"),
  pool_size: 15

config :quilted_memoir, QMES.EventStore,
  serializer: Commanded.Serialization.JsonSerializer,
  username: System.get_env("DATABASE_USER"),
  password: System.get_env("DATABASE_PASS"),
  database: System.get_env("DATABASE_NAME"),
  hostname: System.get_env("DATABASE_HOST"),
  pool_size: 10

config :quilted_memoir, QM.Mailer,
  adapter: Bamboo.LocalAdapter,
  open_email_in_browser_url: "http://localhost:4000/sent_emails"

config :sentry,
  dsn: System.get_env("SENTRY_DSN"),
  environment_name: :prod,
  enable_source_code_context: true,
  root_source_code_path: File.cwd!(),
  tags: %{
    env: "production"
  },
  included_environments: [:prod]
