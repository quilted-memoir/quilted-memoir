use Mix.Config

# We don't run a server during test. If one is required,
# you can enable the server option below.
config :quilted_memoir, QMWeb.Endpoint,
  http: [port: 4002],
  url: [host: "localhost", port: 4002],
  server: false

# Print only warnings and errors during test
config :logger, level: :warn

# Configure your database
config :quilted_memoir, QM.Repo,
  username: System.get_env("DATABASE_USER") || "postgres",
  password: System.get_env("DATABASE_PASS") || "postgres",
  database: System.get_env("DATABASE_NAME") || "quilted_memoir_test",
  hostname: System.get_env("DATABASE_HOST") || "localhost",
  pool_size: 10

config :quilted_memoir, QMES.EventStore,
  serializer: Commanded.Serialization.JsonSerializer,
  column_data_type: "jsonb",
  types: EventStore.PostgresTypes,
  username: System.get_env("DATABASE_USER") || "postgres",
  password: System.get_env("DATABASE_PASS") || "postgres",
  database: System.get_env("DATABASE_NAME") || "quilted_memoir_test",
  hostname: System.get_env("DATABASE_HOST") || "localhost",
  pool_size: 10

config :quilted_memoir, Oban, crontab: false, queues: false, prune: :disabled

config :quilted_memoir, QM.Mailer, adapter: Bamboo.TestAdapter

config :email_checker,
  default_dns: :system,
  also_dns: [],
  validations: [EmailChecker.Check.Format],
  timeout_milliseconds: 4000
