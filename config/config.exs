use Mix.Config

config :quilted_memoir,
  enable_sentry: false,
  namespace: QM,
  ecto_repos: [QM.Repo],
  generators: [binary_id: true],
  event_stores: [QMES.EventStore],
  upload_bucket: "quiltedmemoir"

config :quilted_memoir, QMWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "TpZdbVnGINFpmQAwqB1wC2dDiC+uYqppv/VAV9o7ZMj9e0hXWtwjqIiUqLkzyJaK",
  render_errors: [view: QMWeb.ErrorView, accepts: ~w(html json)],
  pubsub_server: QM.PubSub

config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

config :quilted_memoir, QMWeb.Endpoint,
  live_view: [
    signing_salt: "/cpupAwKrDxjUgX/bAzLGUMUyLYNuGRy"
  ]

config :quilted_memoir, QM.Repo,
  migration_primary_key: [name: :id, type: :binary_id],
  migration_source: "ecto_schema_migrations"

config :commanded,
  registry: Commanded.Registration.LocalRegistry,
  event_store_adapter: QMES.EventStore,
  default_consistency: :strong

config :commanded_ecto_projections,
  repo: QM.Repo

config :quilted_memoir, Oban,
  repo: QM.Repo,
  prune: {:maxlen, 10_000},
  queues: [default: 10, commanded: 50]

config :quilted_memoir, QM.Mailer, adapter: Bamboo.SesAdapter

config :ueberauth, Ueberauth,
  providers: [
    identity:
      {Ueberauth.Strategy.Identity,
       [
         callback_methods: ["POST"],
         uid_field: :email,
         nickname_field: :email,
         param_nesting: "sign_in_form"
       ]}
  ]

config :vex,
  sources: [
    QMES.Support.Validators,
    Vex.Validators
  ]

config :email_checker,
  default_dns: :system,
  also_dns: [],
  validations: [EmailChecker.Check.Format, EmailChecker.Check.MX],
  timeout_milliseconds: 4000

# AWS

config :ex_aws,
  region: "us-west-2",
  access_key_id: [{:system, "AWS_ACCESS_KEY_ID"}, {:awscli, "quiltedmemoir", 30}],
  secret_access_key: [
    {:system, "AWS_SECRET_ACCESS_KEY"},
    {:awscli, "quiltedmemoir", 30}
  ]

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
