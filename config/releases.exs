import Config

config :quilted_memoir,
  enable_sentry: true,
  namespace: QM,
  ecto_repos: [QM.Repo],
  generators: [binary_id: true],
  event_stores: [QMES.EventStore],
  upload_bucket: "quiltedmemoir"

config :quilted_memoir, QMWeb.Endpoint,
  url: [host: "www.quiltedmemoir.com", port: 443, scheme: "https", path: "/"],
  http: [port: System.fetch_env!("PORT") || 4000],
  secret_key_base: System.fetch_env!("PHOENIX_SECRET_KEY_BASE"),
  cache_static_manifest: "priv/static/cache_manifest.json",
  server: true,
  render_errors: [view: QMWeb.ErrorView, accepts: ~w(html json)],
  pubsub_server: QM.PubSub,
  check_origin: [
    "//quiltedmemoir.com",
    "//www.quiltedmemoir.com"
  ]

config :logger, level: :info

config :phoenix, :json_library, Jason

config :quilted_memoir, QM.Repo,
  migration_primary_key: [name: :id, type: :binary_id],
  migration_source: "ecto_schema_migrations",
  username: System.fetch_env!("DATABASE_USER"),
  password: System.fetch_env!("DATABASE_PASS"),
  database: System.fetch_env!("DATABASE_NAME"),
  hostname: System.fetch_env!("DATABASE_HOST"),
  pool_size: 15

config :quilted_memoir, QMES.EventStore,
  serializer: Commanded.Serialization.JsonSerializer,
  column_data_type: "jsonb",
  types: EventStore.PostgresTypes,
  username: System.fetch_env!("DATABASE_USER"),
  password: System.fetch_env!("DATABASE_PASS"),
  database: System.fetch_env!("DATABASE_NAME"),
  hostname: System.fetch_env!("DATABASE_HOST"),
  pool_size: 10

config :commanded,
  registry: Commanded.Registration.LocalRegistry,
  event_store_adapter: QMES.EventStore,
  default_consistency: :strong

config :commanded_ecto_projections,
  repo: QM.Repo

config :quilted_memoir, Oban,
  repo: QM.Repo,
  prune: {:maxlen, 10_000},
  queues: [default: 10, commanded: 50]

config :quilted_memoir, QM.Mailer, adapter: Bamboo.SesAdapter

config :ueberauth, Ueberauth,
  providers: [
    identity:
      {Ueberauth.Strategy.Identity,
       [
         callback_methods: ["POST"],
         uid_field: :email,
         nickname_field: :email,
         param_nesting: "sign_in_form"
       ]}
  ]

config :vex,
  sources: [
    QMES.Support.Validators,
    Vex.Validators
  ]

config :email_checker,
  default_dns: :system,
  also_dns: [],
  validations: [EmailChecker.Check.Format, EmailChecker.Check.MX],
  timeout_milliseconds: 4000

# AWS

config :ex_aws,
  region: "us-west-2",
  access_key_id: [{:system, "AWS_ACCESS_KEY_ID"}, {:awscli, "quiltedmemoir", 30}],
  secret_access_key: [
    {:system, "AWS_SECRET_ACCESS_KEY"},
    {:awscli, "quiltedmemoir", 30}
  ]

config :quilted_memoir, QM.Mailer,
  adapter: Bamboo.SesAdapter,
  ex_aws: [region: "us-west-2"]

config :sentry,
  dsn: System.fetch_env!("SENTRY_DSN"),
  environment_name: :prod,
  enable_source_code_context: true,
  root_source_code_path: File.cwd!(),
  tags: %{
    env: "production"
  },
  included_environments: [:prod]

config :segment,
  api_url: "https://rudderstack.quiltedmemoir.com/v1/",
  write_key: "1d4EDzdr7EzdjxZRp2MiM0R7BYT"
