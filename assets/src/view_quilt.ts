import axios from "axios";

const quiltIdMatcher = new RegExp(
  /\quilt\/([0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12})#?$/i
);

const quiltMatch = window.location.href.match(quiltIdMatcher);

const setPhotoBtn = document.getElementById("set-photo") as HTMLElement;
const setPhotoInput = document.getElementById(
  "set-photo-input"
) as HTMLInputElement;

setPhotoBtn.addEventListener("click", async function(e) {
  if (quiltMatch) document.getElementById("set-photo-input")?.click();
});

setPhotoInput.addEventListener("change", async function(e) {
  try {
    if (!quiltMatch) {
      throw new Error("Unable to determine the quilt ID");
    }
    if (setPhotoInput.files && setPhotoInput.files[0]) {
      const uploadUrl = await axios.post("/api/files");
      const urlToUploadTo = uploadUrl.data.url;

      const file = setPhotoInput.files[0];
      await axios.put(urlToUploadTo, file, {
        headers: {
          "Content-Type": file.type
        }
      });
      const quiltId = quiltMatch[1];

      const url = urlToUploadTo.split("?")[0];

      await axios.post(`/api/quilt/${quiltId}/photo`, { url });

      setTimeout(() => {
        window.location.reload();
      }, 1000);
    }
  } finally {
    setPhotoInput.files = null;
  }
});
