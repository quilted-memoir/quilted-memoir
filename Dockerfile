FROM alpine:3.11

ENV REPLACE_OS_VARS=true
ENV MIX_ENV=prod
ENV INSTALL_PATH=/app
ENV LANG=C.UTF-8

# Install deps
RUN apk add --no-cache \
    openssl \
    ncurses


RUN mkdir -p ${INSTALL_PATH}

WORKDIR ${INSTALL_PATH}

COPY _build/${MIX_ENV}/rel/quilted_memoir/ "${INSTALL_PATH}/"

ENTRYPOINT ["/bin/sh", "-c", "/app/bin/quilted_memoir start"]
