#!/bin/bash

mkdir -p files

if [ ! -f "files/root.key" ]; then
# Create a root certificate and signing key.
step certificate create "Quilted Memoir Dev CA" files/root.crt files/root.key --no-password --force --insecure --profile root-ca

echo "🚧 This prompt is for your administrator password to install the certs into the keychain"
sudo step certificate install files/root.crt
fi

# Create a leaf certificate for our server.
#
# Need --insecure here because we want to encrypt your key with a password,
# but doing so will make it hard to use programatically. So we'll ask for
# no password, but `step` wants the user to acknowledge the risks.

step certificate create doc.quiltedmemoir-dev.com files/doc.crt files/doc.key --ca files/root.crt --ca-key files/root.key --no-password --force --insecure --not-after="8766h"
step certificate create cover.quiltedmemoir-dev.com files/cover.crt files/cover.key --ca files/root.crt --ca-key files/root.key --no-password --force --insecure --not-after="8766h"
step certificate create coverage.quiltedmemoir-dev.com files/coverage.crt files/coverage.key --ca files/root.crt --ca-key files/root.key --no-password --force --insecure --not-after="8766h"
step certificate create app.quiltedmemoir-dev.com files/app.crt files/app.key --ca files/root.crt --ca-key files/root.key --no-password --force --insecure --not-after="8766h"