# Assumes you're in a git repository
export SENTRY_ORG=everflect
VERSION=$(sentry-cli releases propose-version)

# Create a release
sentry-cli releases new -p quilted-memoir $VERSION

# Associate commits with the release
sentry-cli releases set-commits --auto $VERSION