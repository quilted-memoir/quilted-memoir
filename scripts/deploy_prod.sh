#!/bin/bash

cd /opt/docker/app.quiltedmemoir.com

docker-compose pull
docker-compose up --no-start
docker network connect appquiltedmemoircom_default nginx-proxy || echo "Already connected"

# Run migrations

docker run --env-file ./envfile_app \
    --rm \
    --network appquiltedmemoircom_default \
    --link appquiltedmemoircom_db_1:db \
    --entrypoint /app/bin/quilted_memoir \
    registry.gitlab.com/quilted-memoir/quilted-memoir:latest \
    eval "QM.Release.all_tasks"

docker-compose up -d
