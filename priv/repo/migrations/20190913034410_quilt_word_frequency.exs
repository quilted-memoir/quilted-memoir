defmodule QM.Repo.Migrations.QuiltWordFrequency do
  use Ecto.Migration

  def change do
    create table(:quilt_word_frequency) do
      add :quilt_id, :string
      add :word, :string
      add :count, :integer
    end
  end
end
