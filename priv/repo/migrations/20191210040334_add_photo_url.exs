defmodule QM.Repo.Migrations.AddPhotoUrl do
  use Ecto.Migration

  def change do
    alter table("quilts") do
      add :photo_url, :text
    end
  end
end
