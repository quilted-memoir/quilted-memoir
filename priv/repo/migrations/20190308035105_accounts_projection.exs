defmodule QM.Repo.Migrations.AccountsProjection do
  use Ecto.Migration

  def change do
    create table(:accounts) do
      add :name, :string
      add :email, :string
      add :password_hashed, :string
      add :password_reset_token, :string
      add :password_reset_token_expiration, :utc_datetime_usec
    end
  end
end
