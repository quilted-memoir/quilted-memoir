defmodule QM.Repo.Migrations.QuiltProjection do
  use Ecto.Migration

  def change do
    create table(:quilts) do
      add :name, :string
      add :user_id, :string
    end
  end
end
