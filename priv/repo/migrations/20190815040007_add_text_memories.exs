defmodule QM.Repo.Migrations.AddTextMemories do
  use Ecto.Migration

  def change do
    create table(:text_memories) do
      add :quilt_id, :string, null: false
      add :user_id, :string
      add :memory, :text, null: false
    end
  end
end
