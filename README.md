# QM

[![pipeline status](https://gitlab.com/quilted-memoir/quilted-memoir/badges/main/pipeline.svg)](https://gitlab.com/quilted-memoir/quilted-memoir/commits/main) [![coverage report](https://gitlab.com/quilted-memoir/quilted-memoir/badges/main/coverage.svg)](https://gitlab.com/quilted-memoir/quilted-memoir/commits/main)

To start quilted memoir for development:

- Install dependencies with `mix deps.get`
- Install small step SSL CLI `brew install step`
- Switch into the `local_development` directory`
- Generate local certificates `cd ssl && ./bootstrap.sh`
- Run the database and proxy servers with `docker-compose up -d`
- Setup the database with `mix qm.setup`
- Install Node.js dependencies with `cd assets && npm install`
- Start Phoenix endpoint with `iex -S phx.server`
- Update your /etc/hosts or DNS resolver to point the following domains to localhost:
  - app.quiltedmemoir-dev.com
  - cover.quiltedmemoir-dev.com
  - coverage.quiltedmemoir-dev.com
  - doc.quiltedmemoir-dev.com

Now you can visit [`app.quiltedmemoir-dev.com`](http://localhost:4000) from your browser.
