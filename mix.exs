defmodule QM.MixProject do
  use Mix.Project

  def project do
    [
      # Docs
      name: "Quilted Memoir",
      source_url: "https://gitlab.com/quilted-memoir/quilted-memoir",
      homepage_url: "https://www.quiltedmemoir.com",
      docs: [
        # The main page in the docs
        main: "QM",
        extras: ["README.md"]
      ],

      # Configuration
      app: :quilted_memoir,
      version: "0.1.0",
      elixir: "~> 1.5",
      elixirc_paths: elixirc_paths(Mix.env()),
      elixirc_options: [warnings_as_errors: true],
      compilers: [:phoenix, :gettext] ++ Mix.compilers(),
      start_permanent: Mix.env() == :prod,
      aliases: aliases(),
      deps: deps(),
      test_coverage: [tool: ExCoveralls],
      preferred_cli_env: [
        coveralls: :test,
        "coveralls.detail": :test,
        "coveralls.post": :test,
        "coveralls.html": :test,
        quality: :test,
        "quality.ci": :test
      ],
      dialyzer: [
        plt_file: {:no_warn, ".plt/dialyzer.plt"},
        ignore_warnings: ".dialyzer_ignore.exs"
      ]
    ]
  end

  # Configuration for the OTP application.
  #
  # Type `mix help compile.app` for more information.
  def application do
    [
      mod: {QM.Application, []},
      extra_applications: [
        :logger,
        :runtime_tools,
        :os_mon,
        :eventstore,
        :ueberauth,
        :ueberauth_identity,
        :exconstructor,
        :email_checker
      ]
    ]
  end

  # Specifies which paths to compile per environment.
  defp elixirc_paths(:test), do: ["lib", "test/support"]
  defp elixirc_paths(_), do: ["lib"]

  # Specifies your project dependencies.
  #
  # Type `mix help deps` for examples and options.
  defp deps do
    [
      {:argon2_elixir, "~> 2.0"},
      {:bamboo, "~> 1.2"},
      {:bamboo_ses, "~> 0.1.0"},
      {:burnex, "~> 1.1"},
      {:commanded, "~> 1.0"},
      {:commanded_ecto_projections, "~> 1.0"},
      {:commanded_eventstore_adapter, "~> 1.0"},
      {:oban, "~> 1.2"},
      {:configparser_ex, "~> 4.0"},
      {:ecto_sql, "~> 3.0"},
      {:email_checker, "~> 0.1.2"},
      {:ex_aws, "~> 2.1"},
      {:ex_aws_s3, "~> 2.0"},
      {:ex_pwned, "~> 0.2"},
      {:exconstructor, "~> 1.1.0"},
      {:gettext, "~> 0.11"},
      {:hackney, "~> 1.16"},
      {:jason, "~> 1.0"},
      {:password_validator, "~> 0.4.0"},
      {:phoenix, "~> 1.5"},
      {:phoenix_ecto, "~> 4.0"},
      {:phoenix_html, "~> 2.11"},
      {:phoenix_pubsub, "~> 2.0"},
      {:phoenix_live_view, "~> 0.13.2"},
      {:phoenix_live_dashboard, "~> 0.1"},
      {:telemetry_poller, "~> 0.4"},
      {:telemetry_metrics, "~> 0.4"},
      {:plug_cowboy, "~> 2.2"},
      {:postgrex, ">= 0.0.0"},
      {:sentry, "~> 7.0"},
      {:sweet_xml, "~> 0.6"},
      {:timex, "~> 3.0"},
      {:ueberauth_identity, "~> 0.2"},
      {:ueberauth, "~> 0.6"},
      {:vex, "~> 0.8.0"},
      {:segment, git: "https://github.com/brentjanderson/analytics-elixir.git"},
      {:phoenix_live_reload, "~> 1.2", only: :dev},
      {:ex_check, ">= 0.0.0", only: :dev, runtime: false},
      {:faker, "~> 0.12", only: :test},
      {:dialyxir, "~> 1.0.0-rc.6", only: [:dev, :test], runtime: false},
      {:credo, "~> 1.4", only: [:dev, :test], runtime: false},
      {:sobelow, "~> 0.10.2", only: [:dev, :test], runtime: false},
      {:mix_test_watch, "~> 1.0", only: :dev, runtime: false},
      {:excoveralls, "~> 0.13", only: :test},
      {:ex_doc, "~> 0.21", only: :dev, runtime: false},
      {:floki, ">= 0.0.0", only: :test}
    ]
  end

  # Aliases are shortcuts or tasks specific to the current project.
  # For example, to create, migrate and run the seeds file at once:
  #
  #     $ mix ecto.setup
  #
  # See the documentation for `Mix` for more info on aliases.
  defp aliases do
    [
      "ecto.setup": [
        "ecto.create",
        "event_store.setup",
        "ecto.migrate",
        "run priv/repo/seeds.exs"
      ],
      "ecto.reset": ["ecto.drop", "ecto.setup"],
      "event_store.reset": ["event_store.drop", "event_store.setup"],
      "event_store.setup": ["event_store.create", "event_store.init"],
      "qm.reset": ["event_store.drop", "qm.setup"],
      "qm.setup": ["event_store.setup", "ecto.setup"],
      test: ["ecto.setup --quiet", "test"],
      quality: ["format", "credo --strict", "sobelow --verbose", "dialyzer", "test"],
      "quality.ci": [
        "test --cover",
        "format --check-formatted",
        "credo --strict",
        # "sobelow --exit",
        "dialyzer --ignore-exit-status"
      ]
    ]
  end
end
